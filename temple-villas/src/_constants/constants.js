const constants = {
    API_URL: process.env.REACT_APP_ENVIRONMENT === "production" ? process.env.REACT_APP_LIVE_API_URL : process.env.REACT_APP_DEV_API_URL,
    REQUIRE_INTERCEPTORS: {
        requestHandlerEnabled: true,
        responseHandlerEnabled: true
    }
};

export default constants;