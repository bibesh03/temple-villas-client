import axiosInstance from './api.service';
import constants from '../_constants/constants';

const endpointOrigin = "Technician/";

const GetAllTechnicians = async () => await axiosInstance.get(endpointOrigin + 'GetAllTechnicians', constants.REQUIRE_INTERCEPTORS)

const DeleteTechnician = async (technicianId) => await axiosInstance.get(endpointOrigin + 'DeleteTechnician/' + technicianId, constants.REQUIRE_INTERCEPTORS)

const GetTechnicianToEdit = async (technicianId) => await axiosInstance.get(endpointOrigin + 'Edit/' + technicianId, constants.REQUIRE_INTERCEPTORS)

const EditTechnician = async (model) => await axiosInstance.post(endpointOrigin + 'Edit', model, constants.REQUIRE_INTERCEPTORS)

export {
    GetAllTechnicians,
    DeleteTechnician,
    GetTechnicianToEdit,
    EditTechnician
}
