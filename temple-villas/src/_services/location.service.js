import axiosInstance from './api.service';
import constants from '../_constants/constants';

const endpointOrigin = "Location/";

const GetAllLocations = async () => await axiosInstance.get(endpointOrigin + 'GetAllLocations', constants.REQUIRE_INTERCEPTORS)

const DeleteLocation = async (locationId) => await axiosInstance.get(endpointOrigin + 'DeleteLocation/' + locationId, constants.REQUIRE_INTERCEPTORS)

const GetLocationToEdit = async (locationId) => await axiosInstance.get(endpointOrigin + 'Edit/' + locationId, constants.REQUIRE_INTERCEPTORS)

const EditLocation = async (model) => await axiosInstance.post(endpointOrigin + 'Edit', model, constants.REQUIRE_INTERCEPTORS)

export {
    GetAllLocations,
    DeleteLocation,
    GetLocationToEdit,
    EditLocation
}
