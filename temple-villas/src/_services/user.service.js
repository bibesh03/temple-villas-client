import axiosInstance from './api.service';
import constants from '../_constants/constants';

const endpointOrigin = "Users/";

const Authenticate = async (model) => await axiosInstance.post(endpointOrigin + 'Authenticate', model, {
    requestHandlerEnabled: false,
    responseHandlerEnabled: true
})

const GetAllUsers = async () => await axiosInstance.get(endpointOrigin + 'GetAllUser', constants.REQUIRE_INTERCEPTORS)

const RegisterUser = async (model) => await axiosInstance.post(endpointOrigin + 'Register', model, constants.REQUIRE_INTERCEPTORS)

const GetUserToEdit = async (userId) => await axiosInstance.get(endpointOrigin + 'Edit/' + userId, constants.REQUIRE_INTERCEPTORS)

const EditUser = async (model) => await axiosInstance.post(endpointOrigin + 'Edit', model, constants.REQUIRE_INTERCEPTORS)

const ChangePassword = async (userId, pwd) => await axiosInstance.post(endpointOrigin + 'ChangePassword/' + userId + '/' + pwd, {}, constants.REQUIRE_INTERCEPTORS)

const GetNonAdminUsers = async () => await axiosInstance.get(endpointOrigin + 'GetNonAdminUsers', constants.REQUIRE_INTERCEPTORS)

export {
    Authenticate,
    GetAllUsers,
    RegisterUser,
    GetUserToEdit,
    EditUser,
    ChangePassword,
    GetNonAdminUsers
}
