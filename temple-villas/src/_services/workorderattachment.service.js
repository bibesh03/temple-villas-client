import axiosInstance from './api.service';
import constants from '../_constants/constants';

const endpointOrigin = "WorkOrderAttachment/";

const GetAllAttachments = async (workOrderId) => await axiosInstance.get(endpointOrigin + 'GetAllAttachmentsByWorkOrder?workOrderId=' + workOrderId, constants.REQUIRE_INTERCEPTORS)

const DeleteAttachment = async (guid) => await axiosInstance.delete(endpointOrigin + 'DeleteAttachment?guid=' + guid, constants.REQUIRE_INTERCEPTORS)

const SaveAttachment = async (model) => await axiosInstance.post(endpointOrigin + 'SaveAttachment', model, constants.REQUIRE_INTERCEPTORS)

export {
    GetAllAttachments,
    DeleteAttachment,
    SaveAttachment
}
