import axiosInstance from './api.service';
import constants from '../_constants/constants';

const endpointOrigin = "WorkOrderLineItem/";

const GetAllWorkOrderLineItems = async () => await axiosInstance.get(endpointOrigin + 'GetAllWorkOrderLineItems', constants.REQUIRE_INTERCEPTORS)

const DeleteWorkOrderLineItem = async (workOrderLineItemId) => await axiosInstance.get(endpointOrigin + 'DeleteWorkOrderLineItem/' + workOrderLineItemId, constants.REQUIRE_INTERCEPTORS)

const GetWorkOrderLineItemToEdit = async (workOrderLineItemId) => await axiosInstance.get(endpointOrigin + 'Edit/' + workOrderLineItemId, constants.REQUIRE_INTERCEPTORS)

const EditWorkOrderLineItem = async (model) => await axiosInstance.post(endpointOrigin + 'Edit', model, constants.REQUIRE_INTERCEPTORS)

export {
    GetAllWorkOrderLineItems,
    DeleteWorkOrderLineItem,
    GetWorkOrderLineItemToEdit,
    EditWorkOrderLineItem
}
