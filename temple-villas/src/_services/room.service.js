import axiosInstance from './api.service';
import constants from '../_constants/constants';

const endpointOrigin = "Room/";

const GetAllRooms = async () => await axiosInstance.get(endpointOrigin + 'GetAllRooms', constants.REQUIRE_INTERCEPTORS)

const GetRoomsForDropDown = async () => await axiosInstance.get(endpointOrigin + 'GetRoomsForDropDown', constants.REQUIRE_INTERCEPTORS)

const DeleteRoom = async (roomId) => await axiosInstance.get(endpointOrigin + 'DeleteRoom/' + roomId, constants.REQUIRE_INTERCEPTORS)

const GetRoomToEdit = async (roomId) => await axiosInstance.get(endpointOrigin + 'Edit/' + roomId, constants.REQUIRE_INTERCEPTORS)

const EditRoom = async (model) => await axiosInstance.post(endpointOrigin + 'Edit', model, constants.REQUIRE_INTERCEPTORS)

export {
    GetAllRooms,
    DeleteRoom,
    GetRoomToEdit,
    EditRoom,
    GetRoomsForDropDown
}
