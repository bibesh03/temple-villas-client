import axiosInstance from './api.service';
import constants from '../_constants/constants';

const endpointOrigin = "Unit/";

const GetAllUnits = async () => await axiosInstance.get(endpointOrigin + 'GetAllUnits', constants.REQUIRE_INTERCEPTORS)

const GetAllUnitsByLocation = async (locationId) => await axiosInstance.get(endpointOrigin + 'GetAllUnitsByLocation/' + locationId, constants.REQUIRE_INTERCEPTORS)

const DeleteUnit = async (unitId) => await axiosInstance.get(endpointOrigin + 'DeleteUnit/' + unitId, constants.REQUIRE_INTERCEPTORS)

const GetUnitToEdit = async (unitId) => await axiosInstance.get(endpointOrigin + 'Edit/' + unitId, constants.REQUIRE_INTERCEPTORS)

const EditUnit = async (model) => await axiosInstance.post(endpointOrigin + 'Edit', model, constants.REQUIRE_INTERCEPTORS)


export {
    GetAllUnits,
    DeleteUnit,
    GetUnitToEdit,
    EditUnit,
    GetAllUnitsByLocation
}
