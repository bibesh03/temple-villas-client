import axiosInstance from './api.service';
import constants from '../_constants/constants';
const endpointOrigin = "WorkOrder/";

const GetAllWorkOrders = async () => await axiosInstance.get(endpointOrigin + 'GetAllWorkOrders', constants.REQUIRE_INTERCEPTORS)

const GetAllWorkOrdersByType = async (userId, type) => await axiosInstance.get(endpointOrigin + 'GetAllWorkOrdersByType/' + userId + '/' + type, constants.REQUIRE_INTERCEPTORS)

const DeleteWorkOrder = async (workOrderId) => await axiosInstance.get(endpointOrigin + 'DeleteWorkOrder/' + workOrderId, constants.REQUIRE_INTERCEPTORS)

const GetWorkOrderToEdit = async (workOrderId) => await axiosInstance.get(endpointOrigin + 'Edit/' + workOrderId, { ...constants.REQUIRE_INTERCEPTORS, fileHandler: true})

const EditWorkOrder = async (model) => await axiosInstance.post(endpointOrigin + 'Edit', model, constants.REQUIRE_INTERCEPTORS)

const Filter = async (model) => await axiosInstance.post(endpointOrigin + 'Filter', model, constants.REQUIRE_INTERCEPTORS)

export {
    GetAllWorkOrders,
    DeleteWorkOrder,
    GetWorkOrderToEdit,
    EditWorkOrder,
    GetAllWorkOrdersByType,
    Filter
}
