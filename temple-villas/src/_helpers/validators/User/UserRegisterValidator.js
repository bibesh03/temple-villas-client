import * as Yup from "yup";

export default Yup.object().shape({
    firstName: Yup.string()
    .required("First Name is required"),
    lastName: Yup.string()
    .required("Last Name is required"),
    userName: Yup.string()
    .required("Username is required"),
    email: Yup.string()
    .required("Email is required")
    .email('Enter a valid email'),
    password: Yup.string()
    .required('Enter a password'),
    confirmPassword: Yup.string()
    .required('Enter a Confirm Password')
    .oneOf([Yup.ref('password')], "Passwords does not match")
});
  