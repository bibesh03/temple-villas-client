import * as Yup from "yup";

export default Yup.object().shape({

  email: Yup.string()
  .required("Please enter either username or email"),

  password: Yup.string()
  .required('Please enter you password.')
});
  
  