import * as Yup from "yup";

export default Yup.object().shape({
    password: Yup.string()
    .required('Enter a password'),
    confirmPassword: Yup.string()
    .required('Enter Confirm Password')
    .oneOf([Yup.ref('password')], "Passwords does not match")
});
  