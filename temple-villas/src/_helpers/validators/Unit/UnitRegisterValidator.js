import * as Yup from "yup";

export default Yup.object().shape({
    unitName: Yup.string()
    .required("Unit Name is required"),
    locationId: Yup.string()
    .required("Please select a Location to assign to the unit")
});
  