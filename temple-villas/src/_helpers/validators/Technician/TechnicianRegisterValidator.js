import * as Yup from "yup";

export default Yup.object().shape({
    firstName: Yup.string()
    .required("First Name is required"),
    lastName: Yup.string()
    .required("Last Name is required"),
    contactNumber: Yup.string(),
    userId: Yup.string()
    .required("Please select a user to assign as a technician"),
    payRate: Yup.number()
    .required("Pay Rate is required")
    .min(1, "Pay Rate must be greater than $1")
    .positive('Pay Rate Cannot be negative'),
});
  