import * as Yup from "yup";

export default Yup.object().shape({
    roomId: Yup.string(),
    dateStarted: Yup.date(),
    dateCompleted: Yup.date()
    .min(Yup.ref('dateStarted'), "End date must be after start date"),
    technicianId: Yup.number()
    .required("Please select a technician")
    .notOneOf([0], "Please select a technician"),
});
  