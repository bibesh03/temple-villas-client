import * as Yup from "yup";

export default Yup.object().shape({
    name: Yup.string()
    .required("Name of Line Item is Required"),
    cost: Yup.number()
    .required("Cost is required")
    .moreThan(0, "Cost of Item must be more than 0"),
    quantity: Yup.number()
    .required("Quantity is required")
    .moreThan(0, "Quantity Of Item must be greater than 0"),
});
  