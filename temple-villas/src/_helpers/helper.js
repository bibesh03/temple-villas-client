import jwt_decode from "jwt-decode";
import constants from '../_constants/constants';

const GetJsonFromLocalStorage = (key) => JSON.parse(localStorage.getItem(key));

const GetLoggedInUserToken = () => GetJsonFromLocalStorage('user').token;

const SaveItemInLocalStorage = (key, value) => localStorage.setItem(key, JSON.stringify(value));

const IsUserLoggedIn = () => {
    let loggedInUser = GetJsonFromLocalStorage('user');
    if (loggedInUser === null)
        return false;
    try {
        var decoded = jwt_decode(loggedInUser.token);
        
        return decoded.role;
    } catch(ex) {
        return false;
    }
    
}

const LoggedInUserTechnicianId = () => {
    let loggedInUser = GetJsonFromLocalStorage('user');
    if (loggedInUser === null)
        return false;
    try {
        var decoded = jwt_decode(loggedInUser.token);
        
        return decoded.TechnicianId == "" ? -1 : decoded.TechnicianId;
    } catch(ex) {
        return false;
    }
}

const LoggedInUserId = () => {
    let loggedInUser = GetJsonFromLocalStorage('user');
    if (loggedInUser === null)
        return false;
    try {
        var decoded = jwt_decode(loggedInUser.token);
        
        return decoded.UserId;
    } catch(ex) {
        return false;
    }
    
}

const DownloadReport = (id) => window.open(constants.API_URL + `Report/GetWorkOrderPrintOut?workOrderId=${id}&token=${GetJsonFromLocalStorage('user').token}`, '_blank')

export {
    GetJsonFromLocalStorage,
    GetLoggedInUserToken,
    SaveItemInLocalStorage,
    IsUserLoggedIn,
    LoggedInUserTechnicianId,
    LoggedInUserId,
    DownloadReport
}