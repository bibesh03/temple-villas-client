import React, { useState, useEffect } from 'react'
import { Formik } from 'formik'
import UnitRegisterValidator from '../../_helpers/validators/Unit/UnitRegisterValidator'
import { Grid, Button, Form, Header, Dropdown, Icon, Label, Select } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import { EditUnit, GetUnitToEdit } from '../../_services/unit.service';
import { Alert } from '../../_helpers/Alert';
import { GetAllLocations } from '../../_services/location.service';

const Unit = (props) => {
    const [unitToSave, setUnit] = useState({
        id: 0,
        unitName: "",
        locationId: "",
    });
    const [locationOptions, setLocationOptions] = useState([])

    useEffect(() => {
        GetLocationsForDropDown();
    }, []);

    const GetLocationsForDropDown = () => {
        GetAllLocations().then(res => {
            var optionsReadyToSet = res.map(x => {
                return {
                    key: x.id,
                    value: x.id,
                    text: x.name,
                }
            });

            if (props.match.params.id > 0) {
                GetUnitEditData(optionsReadyToSet);
            }
            setLocationOptions(optionsReadyToSet)
        }).catch(ex => { })
    }

    const GetUnitEditData = (optionsReadyToSet) => {
        GetUnitToEdit(props.match.params.id).then(res => {
            setUnit(res);
        }).catch(ex => {
            Alert({ success: false, message: "Unit does not exist" });
            props.history.push('/Units');
        })
    }

    const SaveUnit = (model, setSubmitting) => {
        EditUnit(model).then(res => {
            setSubmitting(false);
            Alert({ success: true, message: res.message });
            props.history.push('/Units');
        }).catch(ex => {
            setSubmitting(false);
        });
    }

    return (
        <>
            <Formik initialValues={unitToSave}
                enableReinitialize
                validationSchema={UnitRegisterValidator}
                onSubmit={(values, { setSubmitting }) => {
                    SaveUnit(values, setSubmitting);
                }}>
                {({ values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting }) => (
                        <React.Fragment>
                            <Header as='h2'>
                                <Header.Content>
                                    {props.match.params.id == 0 ? "Add" : "Edit"} Unit
                                    <Header.Subheader>
                                        <Link to='/Units'> Back to listing</Link>
                                    </Header.Subheader>
                                </Header.Content>
                            </Header>

                            <Form onSubmit={handleSubmit}>
                                <Grid divided='vertically'>
                                    <Grid.Row columns={2}>
                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>Unit Name</label>
                                                    <Form.Input
                                                        error={touched.unitName && errors.unitName}
                                                        fluid
                                                        placeholder='Unit Unit #'
                                                        type='text'
                                                        id='unitName'
                                                        onChange={handleChange}
                                                        value={values.unitName}
                                                        onBlur={handleBlur}
                                                        className={touched.unitName && errors.unitName ? "has-error" : null}
                                                    />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>Location</label>
                                                    <Select
                                                        placeholder='Select a Location'
                                                        fluid
                                                        search
                                                        clearable
                                                        selection
                                                        options={locationOptions}
                                                        error={(touched.locationId && errors.locationId) ? true : false}
                                                        id='locationId'
                                                        name='locationId'
                                                        onChange={(e, elem) => {
                                                            const event = {
                                                                persist: () => { },
                                                                target: {
                                                                    type: "change",
                                                                    id: 'locationId',
                                                                    name: 'locationId',
                                                                    value: elem.value
                                                                }
                                                            };

                                                            handleChange(event);
                                                        }}
                                                        value={values.locationId}
                                                        onBlur={(e, elem) => {
                                                            const event = {
                                                                persist: () => { },
                                                                target: {
                                                                    type: "blur",
                                                                    id: 'locationId',
                                                                    name: 'locationId',
                                                                    value: elem.value
                                                                }
                                                            };

                                                            handleBlur(event);
                                                        }}
                                                    />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                                <Button floated='right' type='submit' color='facebook' size={'medium'} disabled={isSubmitting}><Icon name='save' />Save</Button>
                            </Form>
                        </React.Fragment>
                    )}
            </Formik>
        </>
    );
}
export default Unit;