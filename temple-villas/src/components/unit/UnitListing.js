import React, { useState, useEffect } from 'react';
import { Modal, Icon, Button, Input, List, Form } from 'semantic-ui-react';
import DataTable from 'react-data-table-component';
import { GetAllUnitsByLocation, DeleteUnit, EditUnit, GetUnitToEdit } from '../../_services/unit.service'
import { Alert } from '../../_helpers/Alert';
import { DeleteRoom, EditRoom, GetRoomToEdit } from '../../_services/room.service'
import { Formik } from 'formik';
import UnitRegisterValidator from '../../_helpers/validators/Unit/UnitRegisterValidator';
import RoomValidator from '../../_helpers/validators/Room/RoomValidator';

const UnitListing = (props) => {
    const [unit, setUnitList] = useState([]);
    const [searchableList, setSearchableList] = useState([]);
    const [unitToEdit, setUnitToEdit] = useState({ id: 0, unitName: '' });
    const [roomToEdit, setRoomToEdit] = useState({ id: 0, name: '' });
    const [isUnitEditModalOpen, setIsUnitEditModalOpen] = useState(false);
    const [isRoomEditModalOpen, setIsRoomEditModalOpen] = useState(false);

    useEffect(() => {
        GetUnitListForTable();
    }, [])

    const GetUnitListForTable = () => {
        GetAllUnitsByLocation(props.locationId).then(res => {
            setUnitList(res);
            setSearchableList(res);
        }).catch(ex => {
        });
    }

    const DeleteUnitFromList = (unitId) => {
        DeleteUnit(unitId).then(res => {
            Alert({ success: true, message: "Success! Selected Unit has been deleted." })
            GetUnitListForTable();
        }).catch(ex => {
        });
    }

    const AddUnitToLocation = (unit) => {
        EditUnit(unit).then(res => {
            Alert({ success: true, message: res.message })
            GetUnitListForTable();
        }).catch(ex => { });
    }

    const AddRoomToUnit = (unit) => {
        EditRoom(unit).then(res => {
            Alert({ success: true, message: res.message });
            GetUnitListForTable();
        }).catch(ex => { })
    }

    const DeleteRoomFromModal = (roomId) => {
        DeleteRoom(roomId).then(res => {
            Alert({ success: true, message: res.message });
            GetUnitListForTable();
        }).catch(ex => { })
    }

    const GetUnitToEditForModal = (unitId) => {
        GetUnitToEdit(unitId).then(res => {
            setUnitToEdit(res);
            setIsUnitEditModalOpen(true);
        }).catch(ex => { })
    }

    const GetRoomToEditForModal = (roomId) => {
        GetRoomToEdit(roomId).then(res => {
            setRoomToEdit(res);
            setIsRoomEditModalOpen(true);
        }).catch(ex => { })
    }

    const RoomsTable = (props) => {
        const { data } = props
        useEffect(() => {
        }, [])

        return (
            <div style={{ padding: '16px', paddingLeft: '60px', paddingRight: '60px' }}>
                <br />
                <Button size={'tiny'} color={'instagram'} onClick={() => AddRoomToUnit({
                    id: 0,
                    name: "Room " + (data.rooms.length + 1),
                    unitId: data.id
                })}> <Icon name='add' /> Add Room </Button>
                {data.rooms.length > 0 ?
                    <List verticalAlign={'bottom'} divided>
                        {
                            data.rooms.map(x => <List.Item>
                                <List.Content floated='right' verticalAlign='top'>
                                    <Button color="linkedin" size={'tiny'} onClick={() => {
                                        GetRoomToEditForModal(x.id);
                                    }}>Edit</Button>
                                    <Button color="red" size={'tiny'} onClick={() => DeleteRoomFromModal(x.id)}>Delete</Button>
                                </List.Content>
                                <List.Icon name='building' />
                                <List.Content>{x.name}</List.Content>
                            </List.Item>)
                        }
                    </List> : <p style={{ textAlign: 'center' }}>No rooms available for this Unit</p>
                }

            </div>
        )
    }

    const columns = [
        {
            name: '',
            selector: 'id',
            sortable: true,
            cell: row => (
                <>
                    <Button size={'tiny'} color={'instagram'} onClick={() => {
                        GetUnitToEditForModal(row.id);
                    }}> <Icon name='edit' /><span className="display-on-desktop">Edit</span></Button>
                    <Modal
                        size={"mini"}
                        dimmer={true}
                        open={isUnitEditModalOpen}
                    >
                        <Modal.Header>Unit Details</Modal.Header>
                        <Formik initialValues={unitToEdit}
                            enableReinitialize
                            validationSchema={UnitRegisterValidator}
                            onSubmit={(values, { setSubmitting }) => {
                                AddUnitToLocation(values);
                                setIsUnitEditModalOpen(false);
                                setSubmitting(false);
                            }}>
                            {({ values,
                                errors,
                                touched,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                isSubmitting }) => (
                                    <>
                                        <Modal.Content>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>Unit #</label>
                                                    <Form.Input placeholder='Enter Unit Name'
                                                        error={touched.unitName && errors.unitName}
                                                        fluid
                                                        id='unitName'
                                                        onChange={handleChange}
                                                        value={values.unitName}
                                                        onBlur={handleBlur}
                                                        className={touched.unitName && errors.unitName ? "has-error" : null} />
                                                </Form.Field>
                                            </Form.Group>
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <Button negative onClick={() => {
                                                setIsUnitEditModalOpen(false);
                                            }}>
                                                Close
                                    </Button>
                                            <Button positive disabled={isSubmitting} onClick={handleSubmit}>
                                                Save
                                    </Button>
                                        </Modal.Actions>
                                    </>
                                )}
                        </Formik>
                    </Modal>
                    <Modal
                        trigger={<Button color='google plus' size={'tiny'}><Icon name='trash' /><span className="display-on-desktop">Delete</span></Button>}
                        header='Delete Unit'
                        content='Are you sure you want to delete this Unit? Deleting this unit will delete all the associated rooms within this unit!'
                        actions={['Cancel', { key: 'delete', content: 'Delete', positive: true, onClick: () => DeleteUnitFromList(row.id) }]}
                    />
                </>
            )
        },
        {
            name: 'Unit Name',
            selector: 'unitName',
            sortable: true,
        },
        ,
        {
            name: 'Rooms',
            selector: 'unitName',
            sortable: true,
            cell: row => row.rooms.length
        }
    ];

    return (
        <>
            <Button size={'tiny'} color={'instagram'} onClick={() => AddUnitToLocation({
                id: 0,
                unitName: "Unit " + String.fromCharCode(65 + searchableList.length),
                locationId: props.locationId
            })}> <Icon name='add' /> Add Unit </Button>
            <DataTable
                title={"Units"}
                columns={columns}
                data={searchableList}
                pagination
                paginationPerPage={5}
                subHeader
                expandableRows
                expandableRowsComponent={<RoomsTable />}
                subHeaderAlign="left"
                subHeaderComponent={
                    <React.Fragment>
                        <Input
                            size='small'
                            placeholder='Search by Unit Name...'
                            onChange={(e, data) => setSearchableList(unit.filter(x => x.unitName.toLowerCase().indexOf(data.value.toLowerCase()) > -1))}
                        />
                    </React.Fragment>
                }
            />
            {/* Roome Edit Modal */}
            <Modal
                size={"mini"}
                dimmer={true}
                open={isRoomEditModalOpen}
            >
                <Modal.Header>Room Details</Modal.Header>
                <Formik initialValues={roomToEdit}
                    enableReinitialize
                    validationSchema={RoomValidator}
                    onSubmit={(values, { setSubmitting }) => {
                        AddRoomToUnit(values);
                        setIsRoomEditModalOpen(false);
                        setSubmitting(false);
                    }}>
                    {({ values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting }) => (
                            <>
                                <Modal.Content>
                                    <Form.Group widths='equal'>
                                        <Form.Field>
                                            <label>Room Name</label>
                                            <Form.Input placeholder='Enter Room Name'
                                                error={touched.name && errors.name}
                                                fluid
                                                id='name'
                                                onChange={handleChange}
                                                value={values.name}
                                                onBlur={handleBlur}
                                                className={touched.name && errors.name ? "has-error" : null} />
                                        </Form.Field>
                                    </Form.Group>
                                </Modal.Content>
                                <Modal.Actions>
                                    <Button negative onClick={() => {
                                        setIsRoomEditModalOpen(false);
                                    }}>
                                        Close
                                    </Button>
                                    <Button positive disabled={isSubmitting} onClick={handleSubmit}>
                                        Save
                                    </Button>
                                </Modal.Actions>
                            </>
                        )}
                </Formik>
            </Modal>
        </>
    )
}

export default UnitListing;