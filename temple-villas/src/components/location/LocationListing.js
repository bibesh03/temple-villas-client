import React, { useState, useEffect } from 'react';
import { Form, Modal, Icon, Button, Input } from 'semantic-ui-react';
import DataTable from 'react-data-table-component';
import { GetAllLocations, DeleteLocation, GetLocationToEdit, EditLocation } from '../../_services/location.service';
import { Alert } from '../../_helpers/Alert';
import { Formik } from 'formik';
import LocationValidator from '../../_helpers/validators/Location/LocationValidator';
import UnitListing from '../unit/UnitListing';

const LocationListing = (props) => {
    const [isEditModalOpen, setIsEditModalOpen] = useState(false);
    const [locationList, setLocationList] = useState([]);
    const [searchableList, setSearchableList] = useState([]);
    const [locationToEdit, setLocationToEdit] = useState({ id: 0, name: '' });
    const [isEdit, setIsEdit] = useState(false);

    useEffect(() => {
        GetLocationListForTable();
    }, [])

    const GetLocationListForTable = () => {
        GetAllLocations().then(res => {
            setLocationList(res);
            setSearchableList(res);
        }).catch(ex => {
        });
    }

    const DeleteLocationFromList = (locationId) => {
        DeleteLocation(locationId).then(res => {
            Alert({ success: true, message: "Success! Selected Location has been deleted." })
            GetLocationListForTable();
        }).catch(ex => {
        });
    }

    const SubmitLocationName = (values, setSubmitting) => {
        EditLocation(values).then(res => {
            Alert({ success: true, message: res.message });
            GetLocationListForTable();
            setIsEditModalOpen(false);
            setLocationToEdit({ id: 0, name: '' });
            setSubmitting(false);
        }).catch(ex => {
            setIsEditModalOpen(false);
            setSubmitting(false);
        })
    }

    const GetLocationToEditModal = (locationId) => {
        GetLocationToEdit(locationId).then(res => {
            setLocationToEdit(res);
            setIsEditModalOpen(true);
        }).catch(ex => { })
    }

    const columns = [
        {
            name: '',
            selector: 'id',
            sortable: true,
            cell: row => (
                <>
                    <Button primary size={'tiny'} onClick={() => {
                        setIsEdit(true);
                        GetLocationToEditModal(row.id);
                    }}> <Icon name='edit' /><span className="display-on-desktop">Edit</span></Button>
                    <Modal
                        trigger={<Button color='google plus' size={'tiny'}><Icon name='trash' /><span className="display-on-desktop">Delete</span></Button>}
                        header='Delete Location'
                        content='Are you sure you want to delete this Location? Deleting this location will result in deleting the associated units within this location.'
                        actions={['Cancel', { key: 'delete', content: 'Delete', positive: true, onClick: () => DeleteLocationFromList(row.id) }]}
                    />
                </>
            )
        },
        {
            name: 'Name',
            selector: 'name',
            sortable: true,
        },
        {
            name: 'No. Of Units',
            selector: 'count',
            sortable: true,
        }
    ];

    return (
        <>
            <Button size={'tiny'} floated={'right'} color={'instagram'} onClick={() => {
                setIsEdit(false);
                setIsEditModalOpen(true);
                }}> <Icon name='add' /> Add Location</Button>
            <Modal
                size={isEdit ? "large" : "mini"}
                dimmer={true}
                open={isEditModalOpen}
            >
                <Modal.Header>Location Details</Modal.Header>
                <Formik initialValues={locationToEdit}
                    enableReinitialize
                    validationSchema={LocationValidator}
                    onSubmit={(values, { setSubmitting }) => {
                        SubmitLocationName(values, setSubmitting);
                    }}>
                    {({ values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting }) => (
                            <>
                                <Modal.Content>
                                    <Form.Group widths='equal'>
                                        <Form.Field>
                                            <label>Name</label>
                                            <Form.Input placeholder='Enter Location Name'
                                                error={touched.name && errors.name}
                                                fluid
                                                id='name'
                                                onChange={handleChange}
                                                value={values.name}
                                                onBlur={handleBlur}
                                                className={touched.name && errors.name ? "has-error" : null} />
                                        </Form.Field>
                                    </Form.Group>
                                </Modal.Content>
                                {isEdit ? <Modal.Content>
                                    <UnitListing locationId={locationToEdit.id}/>
                                </Modal.Content> : ""}
                                <Modal.Actions>
                                    <Button negative onClick={() => {
                                        setIsEditModalOpen(false);
                                        setLocationToEdit({ id: 0, name: '' });
                                        GetLocationListForTable();
                                    }}>
                                        Close
                                    </Button>
                                    <Button positive disabled={isSubmitting} onClick={handleSubmit}>
                                        Save
                                    </Button>
                                </Modal.Actions>
                            </>
                        )}
                </Formik>
            </Modal>

            <DataTable
                title={"Locations"}
                columns={columns}
                data={searchableList}
                pagination
                subHeader
                subHeaderAlign="left"
                subHeaderComponent={
                    <React.Fragment>
                        <Input
                            size='small'
                            placeholder='Search by Name...'
                            onChange={(e, data) => setSearchableList(locationList.filter(x => x.name.toLowerCase().indexOf(data.value.toLowerCase()) > -1))}
                        />
                    </React.Fragment>
                }
            />
        </>
    )
}

export default LocationListing;