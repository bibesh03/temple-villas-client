import React, { useEffect, useState } from 'react';
import { Button, Icon, Header, Modal, Image, Loader, Segment, Dimmer, Divider } from 'semantic-ui-react';
import DataTable from 'react-data-table-component';
import { Dashboard as UppyDashboard } from '@uppy/react'
import { SaveAttachment, GetAllAttachments, DeleteAttachment } from '../../_services/workorderattachment.service';
import { Alert } from '../../_helpers/Alert';
import constants from '../../_constants/constants';

import '@uppy/core/dist/style.css'
import '@uppy/dashboard/dist/style.css'

const Uppy = require('@uppy/core')
const Webcam = require('@uppy/webcam')
const moment = require('moment');

const WorkOrderAttachments = (props) => {
    const { loggedInUserId, workOrderId, isCompleted } = props;
    const [open, setOpen] = useState(false)
    const [attachments, setAttachments] = useState([]);
    const [isUploading, setIsUploading] = useState(false);
    const [noOfUploadedFiles, setNoOfUploadedFiles] = useState(0);
    const [totalFiles, setTotalFiles] = useState(1);
    const [uppy, setUppy] = useState(new Uppy({
        debug: true,
        autoProceed: false,
        restrictions: {
            maxNumberOfFiles: 100,
            minNumberOfFiles: 1,
            allowedFileTypes: ['image/*', 'video/*', 'file/*', ".pdf", ".docx"]
        }
    }).use(Webcam));

    useEffect(() => {
        uppy.on('complete', (res) => {
            setIsUploading(true);
            var filesToUpload = uppy.getFiles().map(x => x.data);
            setNoOfUploadedFiles(0);
            setTotalFiles(filesToUpload.length);
            UploadFileToServer(filesToUpload);
        });
        GetAttachmentsForWorkOrder();
    }, []);

    const GetAttachmentsForWorkOrder = () => {
        GetAllAttachments(workOrderId).then(res => {
            setAttachments(res)
        });
    }

    const UploadFileToServer = (filesToUpload) => {
        let promises = [];
        var completedFiles = 0;
        filesToUpload.forEach((file, i) => {
            promises.push(
                SaveAttachment(ConvertJsonToFormData({
                    CreatedBy: loggedInUserId,
                    WorkOrderId: workOrderId,
                    File: file
                })).then(res => {
                    // console.log("Completed files ", completedFiles);
                    ++completedFiles;
                    setNoOfUploadedFiles(noOfUploadedFiles + 1);
                })
            );
        });

        Promise.all(promises).then(x => {
            if (completedFiles === filesToUpload.length) {
                Alert({ success: true, message: "Success! All attachments has been successfully uploaded" });
                setIsUploading(false);
                uppy.cancelAll();
                GetAttachmentsForWorkOrder();
                setOpen(false);
            }
        })

        //refreshFromServer
    }

    const ConvertJsonToFormData = (json) => {
        var form_data = new FormData();
        for (var key in json) {
            form_data.append(key, json[key]);
        }

        return form_data;
    };

    const RemoveAttachment = (guid) => {
        DeleteAttachment(guid).then(res => {
            GetAttachmentsForWorkOrder();
            Alert({ success: true, message: res.message });
        })
    }

    const columns = [
        {
            name: '',
            selector: 'id',
            sortable: true,
            cell: row => <>
                <Button primary size={'tiny'} onClick={() => window.open('http://templeapi.primtek.net' + row.fileLocation, '_blank')}> <Icon name='eye' /><span className="display-on-desktop">View</span></Button>
                { !isCompleted ? <Button color='google plus' size='tiny' onClick={() => RemoveAttachment(row.guid)}> <Icon name='trash' /><span className="display-on-desktop">Remove</span></Button> : <></>}
            </>
        },
        {
            name: 'Name',
            selector: 'fileName',
            sortable: true,
        },
        {
            name: 'Uploaded Date',
            selector: 'uploadedDate',
            sortable: true,
            cell: row => new moment(row.uploadedDate).format("MM/DD/YYYY hh:mm a")
        }
    ];
    return (
        <>
            <Divider />
            <h1>Attachments</h1>
            {
                isCompleted ? <></> :
                    <Modal
                        dimmer='blurring'
                        closeIcon
                        onClose={() => setOpen(false)}
                        onOpen={() => setOpen(true)}
                        open={open}
                        trigger={<Button color="green"><Icon name='upload' /><span className="display-on-desktop">Upload Files</span></Button>}
                    >
                        <Modal.Header>Upload Attachments</Modal.Header>
                        <Modal.Content>
                            {isUploading ? <div style={{ height: '500px', width: '910px' }}><Segment>
                                <Dimmer active inverted>
                                    <Loader size='massive'>Uploading. Please wait {noOfUploadedFiles} / {totalFiles}</Loader>
                                </Dimmer>

                                <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png' />
                                <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png' />
                                <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png' />
                                <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png' />
                                <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png' />
                                <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png' />
                                <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png' />

                            </Segment> </div> :
                                <UppyDashboard
                                    uppy={uppy}
                                    open={open}
                                    height="500px"
                                    width="910px"
                                    plugins={['Webcam']}
                                    showSelectedFiles
                                    closeModalOnClickOutside
                                    showProgressDetails
                                    onRequestClose={() => setOpen(false)}
                                />}
                        </Modal.Content>
                    </Modal>
            }
            <DataTable
                id='workOrderAttachments'
                columns={columns}
                data={attachments}
                pagination
                paginationPerPage={5}
            />
        </>
    )
}

export default WorkOrderAttachments;