import React, { useState, useEffect } from 'react'
import { Formik } from 'formik'
import WorkOrderValidator from '../../_helpers/validators/WorkOrder/WorkOrderValidator';
import { Grid, Button, Form, Header, Select, Icon, Label, Modal, Segment, Dimmer, Loader, Image, Divider } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import { Alert } from '../../_helpers/Alert';
import { DownloadReport, IsUserLoggedIn } from '../../_helpers/helper';
import { DateTimeInput } from 'semantic-ui-calendar-react';
import { GetRoomsForDropDown } from '../../_services/room.service';
import { GetAllTechnicians } from '../../_services/technician.service';
import { EditWorkOrder, GetWorkOrderToEdit } from '../../_services/workorder.service';
import { GetJsonFromLocalStorage, LoggedInUserTechnicianId } from '../../_helpers/helper';
import WorkOrderLineItemValidator from '../../_helpers/validators/WorkOrder/WorkOrderLineItemValidator';
import DataTable from 'react-data-table-component';
import { EditWorkOrderLineItem, DeleteWorkOrderLineItem, GetWorkOrderLineItemToEdit } from '../../_services/workorderlineitem.service';
import ProcessingLoader from '../shared/ProcessingLoader';
import WorkOrderAttachments from './WorkOrderAttachments';

const WorkOrder = (props) => {
    var moment = require('moment');

    const loggedInUserId = GetJsonFromLocalStorage('user').id;
    const [lineItemsForTable, setLineItemsForTable] = useState([]);
    const defaultLineItem = {
        id: 0,
        workOrderId: props.match.params.id,
        quantity: "",
        name: "",
        cost: ""
    };
    const [workOrderToEdit, setWorkOrderToEdit] = useState({
        id: 0,
        technicianId: 0,
        roomId: "",
        dateStarted: "",
        dateCompleted: "",
        propertyWearId: "",
        createdBy: loggedInUserId,
        createdDate: new Date(),
        isCompleted: false,
        description: '',
    });
    const [workOrderLineItem, setWorkOrderLineItem] = useState(defaultLineItem);
    const [isUserAdmin, setIsUserAdmin] = useState(IsUserLoggedIn() === "Admin");
    const [locationOptions, setLocationOptions] = useState([]);
    const [technicianOptions, setTechnicianOptions] = useState([]);
    const loggedInUserTechnicianId = LoggedInUserTechnicianId();
    const [processing, setProcessing] = useState(true);

    useEffect(() => {
        GetRoomsForDropDownForForm();
        GetTechniciansForForm();
        if (props.match.params.id > 0)
            GetWorkOrderData();
        else
            setProcessing(false);
    }, []);

    const GetWorkOrderData = () => {
        GetWorkOrderToEdit(props.match.params.id).then(res => {
            setWorkOrderToEdit({
                id: res.id,
                technicianId: res.technicianId,
                roomId: res.roomId ? res.roomId : "",
                dateStarted: res.dateStarted ? moment(res.dateStarted).format('MM/DD/YYYY hh:mm a') : "",
                dateCompleted: res.dateCompleted ? moment(res.dateCompleted).format('MM/DD/YYYY hh:mm a') : "",
                isCompleted: res.isCompleted,
                createdBy: res.createdBy,
                createdDate: res.createdDate,
                propertyWearId: res.propertyWearId,
                workOrderCompletedDateTime: res.workOrderCompletedDateTime ? moment(res.workOrderCompletedDateTime).format('MM/DD/YYYY hh:mm a') : "",
                description: res.description
            });
            setLineItemsForTable(res.workOrderLineItems);
            setProcessing(false);
        })
    }

    const GetTechniciansForForm = () => {
        GetAllTechnicians().then(res => {
            setTechnicianOptions(res.map(x => {
                return {
                    key: x.id,
                    value: x.id,
                    text: x.firstName + " " + x.lastName,
                }
            }));
        })
    }

    const GetRoomsForDropDownForForm = () => {
        GetRoomsForDropDown().then(res => {
            setLocationOptions(res.map(x => {
                return {
                    key: x.id,
                    value: x.id,
                    text: x.name,
                }
            }));
        })
    }

    const SaveWorkOrderForm = (model, setSubmitting) => {
        EditWorkOrder(model).then(res => {
            Alert({ success: true, message: res.message });
            setSubmitting(false);

            if (props.match.params.id == 0)
                props.history.push('/');
        }).catch(ex => {
            setSubmitting(false);
        })
    }

    const SaveWorkOrderLineItemForm = (model, setSubmitting, resetForm) => {
        EditWorkOrderLineItem(model).then(res => {
            resetForm(defaultLineItem);
            GetWorkOrderData();
            Alert({ success: true, message: res.message });
            setSubmitting(false);
        }).catch(ex => {
            setSubmitting(false);
        })
    }

    const RemoveLineItem = (workOrderLineItemId) => {
        DeleteWorkOrderLineItem(workOrderLineItemId).then(res => {
            GetWorkOrderData();
            Alert({ success: true, message: res.message });
        }).catch(ex => { })
    }

    const SetWorkOrderToEdit = (workOrderLineItemId) => {
        GetWorkOrderLineItemToEdit(workOrderLineItemId).then(res => {
            setWorkOrderLineItem(res);
        }).catch(ex => { })
    }

    const CompleteWorkOrder = () => {
        let completedForm = workOrderToEdit;
        completedForm['workOrderCompletedDateTime'] = moment().format('MM/DD/YYYY hh:mm a');
        completedForm['isCompleted'] = true;

        EditWorkOrder(completedForm).then(res => {
            Alert({ success: true, message: "Work Order has been marked as completed" });
            props.history.push('/');
        }).catch(ex => { })
    }

    const columns = [
        {
            name: '',
            selector: 'id',
            sortable: true,
            cell: row => <>
                <Button primary size={'tiny'} onClick={() => SetWorkOrderToEdit(row.id)}> <Icon name='edit' /><span className="display-on-desktop">Edit</span></Button>
                <Button color='google plus' size='tiny' onClick={() => RemoveLineItem(row.id)}> <Icon name='trash' /><span className="display-on-desktop">Remove</span></Button>
            </>
        },
        {
            name: 'Name',
            selector: 'name',
            sortable: true,
        },
        {
            name: 'Quantity',
            selector: 'quantity',
            sortable: true,
        },
        {
            name: 'Cost',
            selector: 'cost',
            sortable: true,
            cell: row => row.cost ? '$ ' + row.cost : ''
        },
        {
            name: 'Total',
            selector: 'total',
            sortable: true,
            cell: row => row.cost ? '$ ' + (row.cost * row.quantity).toFixed(2) : '-'
        },
    ];

    const returnFirstRowRemovedArray = () => {
        columns.shift();
        return columns;
    }

    return (
        <>
            {
                processing ?
                    <ProcessingLoader />
                    :
                    <>
                        <Formik initialValues={workOrderToEdit}
                            validationSchema={WorkOrderValidator}
                            enableReinitialize
                            onSubmit={(values, { setSubmitting }) => {
                                SaveWorkOrderForm(values, setSubmitting);
                            }}>
                            {({ values,
                                errors,
                                touched,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                resetForm,
                                isSubmitting }) => (
                                    <React.Fragment>
                                        <Header as='h2'>
                                            <Header.Content>
                                                {workOrderToEdit.id > 0 ? <>Work Order #{(workOrderToEdit.id + "").padStart(7, '0')}</> : <>Create Work Order</>}
                                                <Header.Subheader>
                                                    <Link to={isUserAdmin ? '/' : '/TechnicianDashboard'}> Back to Listing</Link>
                                                </Header.Subheader>
                                            </Header.Content>
                                        </Header>
                                        <Form onSubmit={handleSubmit} style={{ marginBottom: '55px' }}>
                                            <Grid>
                                                <Grid.Row columns={2}>
                                                    <Grid.Column>
                                                        <Form.Group widths='equal'>
                                                            <Form.Field>
                                                                <label>Start Date/Time</label>
                                                                <DateTimeInput
                                                                    dateFormat="MM/DD/YYYY"
                                                                    timeFormat="ampm"
                                                                    placeholder="Select Started Date/Time"
                                                                    clearable
                                                                    onChange={(e, elem) => {
                                                                        const event = {
                                                                            persist: () => { },
                                                                            target: {
                                                                                type: "change",
                                                                                id: 'dateStarted',
                                                                                name: 'dateStarted',
                                                                                value: (elem.value === null) ? "" : elem.value
                                                                            }
                                                                        };

                                                                        handleChange(event);
                                                                    }}
                                                                    value={values.dateStarted}
                                                                    error={(touched.dateStarted && errors.dateStarted) ? true : false}
                                                                    fluid
                                                                    id='dateStarted'
                                                                    className={touched.dateStarted && errors.dateStarted ? "has-error" : null}
                                                                />
                                                                {
                                                                    (touched.dateStarted && errors.dateStarted) ?
                                                                        <Label pointing prompt>{touched.dateStarted && errors.dateStarted}</Label> : ""
                                                                }
                                                            </Form.Field>
                                                        </Form.Group>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <Form.Group widths='equal'>
                                                            <Form.Field>
                                                                <label>Completed Date/Time</label>
                                                                <DateTimeInput
                                                                    dateFormat="MM/DD/YYYY"
                                                                    placeholder="Select Completed Date/Time"
                                                                    timeFormat="ampm"
                                                                    clearable
                                                                    onChange={(e, elem) => {
                                                                        const event = {
                                                                            persist: () => { },
                                                                            target: {
                                                                                type: "change",
                                                                                id: 'dateCompleted',
                                                                                name: 'dateCompleted',
                                                                                value: (elem.value === null) ? "" : elem.value
                                                                            }
                                                                        };

                                                                        handleChange(event);
                                                                    }}
                                                                    value={values.dateCompleted}
                                                                    error={(touched.dateCompleted && errors.dateCompleted) ? true : false}
                                                                    fluid
                                                                    id='dateCompleted'
                                                                    className={touched.dateCompleted && errors.dateCompleted ? "has-error" : null}
                                                                />
                                                                {
                                                                    (touched.dateCompleted && errors.dateCompleted) ?
                                                                        <Label pointing prompt>{touched.dateCompleted && errors.dateCompleted}</Label> : ""
                                                                }
                                                            </Form.Field>
                                                        </Form.Group>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <Form.Group widths='equal'>
                                                            <Form.Field>
                                                                <label>Location</label>
                                                                <Select
                                                                    placeholder='Select a Location'
                                                                    fluid
                                                                    search
                                                                    clearable
                                                                    disabled={values.isCompleted}
                                                                    selection
                                                                    options={locationOptions}
                                                                    error={(touched.roomId && errors.roomId) ? true : false}
                                                                    onChange={(e, elem) => {
                                                                        const event = {
                                                                            persist: () => { },
                                                                            target: {
                                                                                type: "change",
                                                                                id: 'roomId',
                                                                                name: 'roomId',
                                                                                value: elem.value
                                                                            }
                                                                        };

                                                                        handleChange(event);
                                                                    }}
                                                                    value={values.roomId}
                                                                    onBlur={(e, elem) => {
                                                                        const event = {
                                                                            persist: () => { },
                                                                            target: {
                                                                                type: "blur",
                                                                                id: 'roomId',
                                                                                name: 'roomId',
                                                                                value: elem.value
                                                                            }
                                                                        };

                                                                        handleBlur(event);
                                                                    }}
                                                                />
                                                            </Form.Field>
                                                        </Form.Group>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <Form.Group widths='equal'>
                                                            <Form.Field>
                                                                <label>Technician</label>
                                                                <Select
                                                                    placeholder='Select a Technician'
                                                                    fluid
                                                                    search
                                                                    clearable
                                                                    disabled={loggedInUserTechnicianId == values.technicianId || values.isCompleted}
                                                                    selection
                                                                    options={technicianOptions}
                                                                    error={(touched.technicianId && errors.technicianId) ? true : false}
                                                                    onChange={(e, elem) => {
                                                                        const event = {
                                                                            persist: () => { },
                                                                            target: {
                                                                                type: "change",
                                                                                id: 'technicianId',
                                                                                name: 'technicianId',
                                                                                value: elem.value
                                                                            }
                                                                        };

                                                                        handleChange(event);
                                                                    }}
                                                                    value={values.technicianId}
                                                                    onBlur={(e, elem) => {
                                                                        const event = {
                                                                            persist: () => { },
                                                                            target: {
                                                                                type: "blur",
                                                                                id: 'technicianId',
                                                                                name: 'technicianId',
                                                                                value: elem.value
                                                                            }
                                                                        };

                                                                        handleBlur(event);
                                                                    }}
                                                                />
                                                            </Form.Field>
                                                        </Form.Group>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <Form.Group widths='equal'>
                                                            <Form.Field>
                                                                <label>PropertyWearId</label>
                                                                <Form.Input placeholder='Enter PropertywearId'
                                                                    error={touched.propertyWearId && errors.propertyWearId}
                                                                    fluid
                                                                    type='text'
                                                                    disabled={values.isCompleted}
                                                                    id='propertyWearId'
                                                                    onChange={handleChange}
                                                                    value={values.propertyWearId}
                                                                    onBlur={handleBlur}
                                                                    className={touched.propertyWearId && errors.propertyWearId ? "has-error" : null} />
                                                            </Form.Field>
                                                        </Form.Group>
                                                    </Grid.Column>

                                                </Grid.Row>
                                                <Grid.Row columns={1}>
                                                    <Grid.Column>
                                                        <Form.Group widths='equal'>
                                                            <Form.Field>
                                                                <label>Description</label>
                                                                <Form.TextArea placeholder='Enter text...'
                                                                    error={touched.description && errors.description}
                                                                    type='text'
                                                                    disabled={values.isCompleted}
                                                                    id='description'
                                                                    onChange={handleChange}
                                                                    value={values.description}
                                                                    onBlur={handleBlur}
                                                                    className={touched.description && errors.description ? "has-error" : null} />
                                                            </Form.Field>
                                                        </Form.Group>
                                                    </Grid.Column>
                                                </Grid.Row>
                                            </Grid>
                                            {
                                                !values.isCompleted ?
                                                    <Button type='submit' color='linkedin' size={'medium'} disabled={isSubmitting} floated='right'>
                                                        {
                                                            workOrderToEdit.id == 0 ? <><Icon name='add' /> Add </> : <><Icon name='save' /> Save </>
                                                        }
                                                    </Button> : ""
                                            }
                                        </Form>
                                    </React.Fragment>
                                )}
                        </Formik>
                        {
                            props.match.params.id != 0 ?
                                <>
                                    <WorkOrderAttachments isCompleted={workOrderToEdit.isCompleted} loggedInUserId={loggedInUserId} workOrderId={props.match.params.id} />
                                    <Divider />
                                    <h1>Line Items</h1>
                                    {!workOrderToEdit.isCompleted ?
                                        <Formik initialValues={workOrderLineItem}
                                            validationSchema={WorkOrderLineItemValidator}
                                            enableReinitialize
                                            onSubmit={(values, { setSubmitting, resetForm }) => {
                                                SaveWorkOrderLineItemForm(values, setSubmitting, resetForm);
                                            }}>
                                            {({ values,
                                                errors,
                                                touched,
                                                handleChange,
                                                handleBlur,
                                                handleSubmit,
                                                resetForm,
                                                isSubmitting }) => (
                                                    <React.Fragment>
                                                        <Form onSubmit={handleSubmit}>
                                                            <Grid divided='vertically'>
                                                                <Grid.Row columns={4}>
                                                                    <Grid.Column>
                                                                        <Form.Group widths='equal'>
                                                                            <Form.Field>
                                                                                <label>Name</label>
                                                                                <Form.Input placeholder='Name'
                                                                                    error={touched.name && errors.name}
                                                                                    fluid
                                                                                    id='name'
                                                                                    onChange={handleChange}
                                                                                    value={values.name}
                                                                                    onBlur={handleBlur}
                                                                                    className={touched.name && errors.name ? "has-error" : null} />
                                                                            </Form.Field>
                                                                        </Form.Group>
                                                                    </Grid.Column>
                                                                    <Grid.Column>
                                                                        <Form.Group widths='equal'>
                                                                            <Form.Field>
                                                                                <label>Quantity</label>
                                                                                <Form.Input placeholder='Quantity'
                                                                                    error={touched.quantity && errors.quantity}
                                                                                    fluid
                                                                                    type='number'
                                                                                    id='quantity'
                                                                                    onChange={handleChange}
                                                                                    value={values.quantity}
                                                                                    onBlur={handleBlur}
                                                                                    className={touched.quantity && errors.quantity ? "has-error" : null} />
                                                                            </Form.Field>
                                                                        </Form.Group>
                                                                    </Grid.Column>
                                                                    <Grid.Column>
                                                                        <Form.Group widths='equal'>
                                                                            <Form.Field>
                                                                                <label>Cost</label>
                                                                                <Form.Input placeholder='Cost'
                                                                                    error={touched.cost && errors.cost}
                                                                                    fluid
                                                                                    type='number'
                                                                                    id='cost'
                                                                                    onChange={handleChange}
                                                                                    value={values.cost}
                                                                                    onBlur={handleBlur}
                                                                                    className={touched.cost && errors.cost ? "has-error" : null} />
                                                                            </Form.Field>
                                                                        </Form.Group>
                                                                    </Grid.Column>
                                                                    <Grid.Column style={{ padding: '23px' }}>
                                                                        <Form.Group widths='equal'>
                                                                            <Button type='submit' color='linkedin' size={'medium'} disabled={isSubmitting} floated='right'>
                                                                                {workOrderLineItem.id == 0 ? <><Icon name='plus' /> Add </> : <><Icon name='save' /> Save </>}
                                                                            </Button>
                                                                            <Button type='button' onClick={() => {
                                                                                setWorkOrderLineItem(defaultLineItem);
                                                                                resetForm(defaultLineItem);
                                                                            }} color='google plus' size={'medium'} floated='right'> <Icon name='cancel' /> Clear</Button>
                                                                        </Form.Group>
                                                                    </Grid.Column>
                                                                </Grid.Row>
                                                            </Grid>
                                                        </Form>
                                                    </React.Fragment>
                                                )}
                                        </Formik>
                                        : ""
                                    }
                                    <DataTable
                                        id='workOrderLineItemTable'
                                        columns={workOrderToEdit.isCompleted ? returnFirstRowRemovedArray() : columns}
                                        data={lineItemsForTable}
                                        pagination
                                        paginationPerPage={5}
                                    />
                                </> : ""
                        }
                        <div style={{ marginBottom: '50px'}}>
                            {
                                workOrderToEdit.isCompleted ?
                                    <>
                                        <Label as='a' color='teal'>
                                            Work Order Completed at {workOrderToEdit.workOrderCompletedDateTime}
                                        </Label>
                                        <Button onClick={() => DownloadReport(props.match.params.id)} size={'mini'} secondary> <Icon name='download' /> Download Pdf</Button>
                                    </>
                                    : workOrderToEdit.id > 0 ?
                                        <Modal
                                            trigger={<Button color="green"> <Icon name='check' /> Complete Work Order</Button>}
                                            header='Complete Work Order'
                                            content='Are you sure you want to complete this work Order? Once Set complete work order cannot be changed back.'
                                            actions={['Cancel', { key: 'complete', content: 'Mark as Complete', positive: true, onClick: () => CompleteWorkOrder() }]}
                                        /> : ""
                            }
                        </div>
                    </>
            }
        </>
    );
}
export default WorkOrder;