import React, { useEffect, useState } from 'react';
import DataTable from 'react-data-table-component';
import { Icon, Button, Input, Form, Grid, Dropdown, Loader } from 'semantic-ui-react';
import { Filter } from '../../_services/workorder.service';
import { GetAllTechnicians } from '../../_services/technician.service';
import { DatesRangeInput } from 'semantic-ui-calendar-react';
import { GetRoomsForDropDown } from '../../_services/room.service';
import ProcessingLoader from '../shared/ProcessingLoader';

const Report = (props) => {
    const [workOrderList, setWorkOrderList] = useState([]);
    const [searchableWorkOrderList, setSearchableWorkOrderList] = useState([]);
    const [technicianOptions, setTechnicianOptions] = useState([]);
    const [roomOptions, setRoomOptions] = useState([]);
    const defaultModel = {
        technicianIds: [],
        roomIds: [],
        dateRange: ''
    };
    const [filterModel, setFilerModel] = useState(defaultModel);
    const [processing, setProcessing] = useState(true);

    useEffect(() => {
        FilterWorkOrder(filterModel);
        GetAllTechniciansForSearch();
        GetAllRoomsForSearch();
    }, [])

    const FilterWorkOrder = (model) => {
        setProcessing(true);
        model = model ? model : filterModel
        Filter(model).then(res => {
            setWorkOrderList(res);
            setSearchableWorkOrderList(res);
            setProcessing(false);
        }).catch(ex => { })
    }

    const GetAllTechniciansForSearch = () => {
        GetAllTechnicians().then(res => {
            setTechnicianOptions(res.map(x => {
                return {
                    key: x.id,
                    value: x.id,
                    text: x.firstName + " " + x.lastName,
                }
            }));
        })
    }

    const GetAllRoomsForSearch = () => {
        GetRoomsForDropDown().then(res => {
            setRoomOptions(res.map(x => {
                return {
                    key: x.id,
                    value: x.id,
                    text: x.name,
                }
            }));
        })
    }

    const handleFilterValueChange = (elem, value) => {
        var tempFilterModel = { ...filterModel };
        tempFilterModel[elem] = value;

        setFilerModel(tempFilterModel);
    }

    const columns = [
        {
            name: '',
            selector: 'id',
            sortable: true,
            cell: row => <>
                <Button primary size={'tiny'} onClick={() => props.history.push({
                    pathname: 'WorkOrder/' + row.id
                })}> <Icon name='eye' /> View
                </Button>
            </>
        },
        {
            name: 'Work Order#',
            selector: 'workOrderId',
            sortable: true,
        },
        {
            name: 'Technician',
            selector: 'technician',
            sortable: true,
        },
        {
            name: 'Location',
            selector: 'location',
            grow: 2,
            sortable: true,
        },
        {
            name: 'Date Started',
            selector: 'dateStarted',
            sortable: true,
        },
        {
            name: 'Date Completed',
            selector: 'dateCompleted',
            sortable: true,
        },
        {
            name: 'Rate',
            selector: 'rate',
            sortable: true,
            cell: row => row.rate ? '$' + row.rate : "-"
        },
        {
            name: 'Submitted Date',
            selector: 'completedDate',
            sortable: true,
        }
    ];

    return (
        <>
            <Form onSubmit={() => FilterWorkOrder(filterModel)}>
                <Grid divided='vertically'>
                    <Grid.Row columns={3}>
                        <Grid.Column>
                            <Form.Group widths='equal'>
                                <Form.Field>
                                    <label>Technicians {filterModel.clearCount}</label>
                                    <Dropdown
                                        placeholder='Select Technicians'
                                        fluid
                                        multiple
                                        search
                                        clearable
                                        selection
                                        value={filterModel.technicianIds || []}
                                        options={technicianOptions}
                                        onChange={(e, elem) => handleFilterValueChange('technicianIds', elem.value)}
                                    />
                                </Form.Field>
                            </Form.Group>
                        </Grid.Column>
                        <Grid.Column>
                            <Form.Group widths='equal'>
                                <Form.Field>
                                    <label>Rooms</label>
                                    <Dropdown
                                        placeholder='Select Rooms'
                                        fluid
                                        multiple
                                        clearable
                                        search
                                        selection
                                        value={filterModel.roomIds || []}
                                        options={roomOptions}
                                        onChange={(e, elem) => handleFilterValueChange('roomIds', elem.value)}
                                    />
                                </Form.Field>
                            </Form.Group>
                        </Grid.Column>
                        <Grid.Column>
                            <Form.Group widths='equal'>
                                <Form.Field>
                                    <label>Date Range</label>
                                    <DatesRangeInput
                                        value={filterModel.dateRange}
                                        dateFormat="MM/DD/YYYY"
                                        placeholder="From - To"
                                        onChange={(e, elem) => handleFilterValueChange('dateRange', elem.value)}
                                        dateRange
                                    />
                                </Form.Field>
                            </Form.Group>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <Button type='button' color='google plus' size={'medium'} floated='right' onClick={() => {
                    FilterWorkOrder(defaultModel);
                    setFilerModel(defaultModel);
                }}> <Icon name='close' /> Clear</Button>
                <Button type='submit' color='linkedin' size={'medium'} floated='right'> <Icon name='search' /> Filter</Button>
            </Form>

            <DataTable
                title={"Work Orders"}
                columns={columns}
                data={searchableWorkOrderList}
                pagination
                subHeader
                subHeaderAlign="left"
                progressPending={processing}
                progressComponent={<Loader active inline='centered' />}
                subHeaderComponent={
                    <React.Fragment>
                        <Input
                            size='small'
                            placeholder='Search by Work Order #'
                            onChange={(e, data) => setSearchableWorkOrderList(workOrderList.filter(x => x.workOrderId.toLowerCase().indexOf(data.value.toLowerCase()) > -1))}
                        />
                    </React.Fragment>
                }
            />

        </>
    )
}

export default Report;