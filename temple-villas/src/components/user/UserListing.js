import React, { useState, useEffect } from 'react';
import { Container, Header, Icon, Button, Input } from 'semantic-ui-react';
import DataTable from 'react-data-table-component';
import { GetAllUsers } from '../../_services/user.service';

const UserListing = (props) => {
    const [userList, setUserList] = useState([]);
    const [searchableList, setSearchableList] = useState([]);
    useEffect(() => {
        GetAllUsers().then(res => {
            setUserList(res);
            setSearchableList(res);
        }).catch(ex => {

        });
    }, [])

    const columns = [
        {
            name: '',
            selector: 'id',
            sortable: true,
            cell: row => <Button primary size={'tiny'} onClick={() => props.history.push({
                pathname: 'User/' + row.id
            })}> <Icon name='edit' /> Edit</Button>
        },
        {
            name: 'First Name',
            selector: 'firstName',
            sortable: true,
        },
        {
            name: 'Last Name',
            selector: 'lastName',
            sortable: true,
        },
        {
            name: 'User Name',
            selector: 'userName',
            sortable: true,
        },
        {
            name: 'Email',
            selector: 'email',
            sortable: true,
        },
        {
            name: 'IsAdmin',
            selector: 'isadmin',
            sortable: false,
            cell: row => <Icon name={row.isUserAdmin ? 'check' : 'close'} />
        },
        {
            name: 'IsActive',
            selector: 'isActive',
            sortable: false,
            cell: row => <Icon name={row.isActive ? 'check' : 'close'} />
        },
    ];

    return (
        <>
            <Button size={'tiny'} floated={'right'} color={'instagram'} onClick={() => props.history.push('/Register')}> <Icon name='add' /> Add User</Button>
            <DataTable
                title={"Users"}
                columns={columns}
                data={searchableList}
                pagination
                subHeader
                subHeaderAlign="left"
                subHeaderComponent={
                    <React.Fragment>
                        <Input
                            size='small'
                            placeholder='Search by First Name...'
                            onChange={(e, data) => setSearchableList(userList.filter(x => x.firstName.toLowerCase().indexOf(data.value.toLowerCase()) > -1))}
                        />
                    </React.Fragment>
                }
            />
        </>
    )
}

export default UserListing;