import React, { useState, useEffect } from 'react'
import { Formik } from 'formik'
import UserRegisterValidator from '../../_helpers/validators/User/UserRegisterValidator'
import { Grid, Button, Form, Header, Container, Icon } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import { RegisterUser } from '../../_services/user.service';
import { Alert } from '../../_helpers/Alert';

const Register = (props) => {
    const [userToRegister, setUserToRegister] = useState({
        firstName: "",
        lastName: "",
        userName: "",
        email: "",
        password: "",
        confirmPassword: "",
        isActive: false,
        isUserAdmin: false
    });

    useEffect(() => {
    }, []);

    const registerUser = (newUser, setSubmitting) => {
        RegisterUser(newUser).then(res => {
            setSubmitting(false);
            Alert({ success: true, message: res.message});
            props.history.push('/Users');
        }).catch(ex => {
            setSubmitting(false);
        });
    }
    return (
        <>
            <Formik initialValues={userToRegister}
                validationSchema={UserRegisterValidator}
                onSubmit={(values, { setSubmitting }) => {
                    setUserToRegister(values);
                    registerUser(values, setSubmitting);
                }}>
                {({ values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting }) => (
                        <React.Fragment>
                            <Header as='h2'>
                                <Header.Content>
                                    Register User
                  <Header.Subheader>
                                        <Link to='/Users'> Back to listing</Link>
                                    </Header.Subheader>
                                </Header.Content>
                            </Header>

                            <Form onSubmit={handleSubmit}>
                                <Grid divided='vertically'>
                                    <Grid.Row columns={2}>
                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>FirstName</label>
                                                    <Form.Input
                                                        error={touched.firstName && errors.firstName}
                                                        fluid
                                                        placeholder='FirstName'
                                                        type='text'
                                                        id='firstName'
                                                        onChange={handleChange}
                                                        value={values.firstName}
                                                        onBlur={handleBlur}
                                                        className={touched.firstName && errors.firstName ? "has-error" : null}
                                                    />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>LastName</label>
                                                    <Form.Input placeholder='LastName'
                                                        error={touched.lastName && errors.lastName}
                                                        fluid
                                                        type='text'
                                                        id='lastName'
                                                        onChange={handleChange}
                                                        value={values.lastName}
                                                        onBlur={handleBlur}
                                                        className={touched.lastName && errors.lastName ? "has-error" : null} />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>Username</label>
                                                    <Form.Input placeholder='Username'
                                                        error={touched.userName && errors.userName}
                                                        fluid
                                                        type='text'
                                                        id='userName'
                                                        onChange={handleChange}
                                                        value={values.username}
                                                        onBlur={handleBlur}
                                                        className={touched.username && errors.username ? "has-error" : null} />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>Email</label>
                                                    <Form.Input placeholder='Email'
                                                        error={touched.email && errors.email}
                                                        fluid
                                                        type='text'
                                                        id='email'
                                                        onChange={handleChange}
                                                        value={values.email}
                                                        onBlur={handleBlur}
                                                        className={touched.email && errors.email ? "has-error" : null} />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>Password</label>
                                                    <Form.Input placeholder='Password'
                                                        error={touched.password && errors.password}
                                                        fluid
                                                        type='text'
                                                        id='password'
                                                        onChange={handleChange}
                                                        value={values.password}
                                                        onBlur={handleBlur}
                                                        className={touched.password && errors.password ? "has-error" : null} />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>ConfirmPassword</label>
                                                    <Form.Input placeholder='ConfirmPassword'
                                                        error={touched.confirmPassword && errors.confirmPassword}
                                                        fluid
                                                        type='password'
                                                        id='confirmPassword'
                                                        onChange={handleChange}
                                                        value={values.confirmPassword}
                                                        onBlur={handleBlur}
                                                        className={touched.confirmPassword && errors.confirmPassword ? "has-error" : null} />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <Form.Field>
                                                        <Form.Checkbox
                                                            label='Is Admin?'
                                                            id='isUserAdmin'
                                                            onChange={handleChange}
                                                        />
                                                    </Form.Field>
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <Form.Field>
                                                        <Form.Checkbox
                                                            label='Is Active?'
                                                            id='isActive'
                                                            onChange={handleChange}
                                                        />
                                                    </Form.Field>
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                                <Button floated='right' type='submit' color='facebook' size={'medium'} disabled={isSubmitting}><Icon name='plus' />Register</Button>
                            </Form>
                        </React.Fragment>
                    )}
            </Formik>
        </>
    );
}
export default Register;
