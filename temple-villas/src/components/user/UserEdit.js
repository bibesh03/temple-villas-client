import React, { useState, useEffect } from 'react'
import { Formik } from 'formik'
import ChangePasswordValidator from '../../_helpers/validators/User/ChangePasswordValidator';
import { Grid, Button, Form, Header, Container, Icon, Modal } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import { GetUserToEdit, EditUser, ChangePassword } from '../../_services/user.service';
import { Alert } from '../../_helpers/Alert';
import { GetJsonFromLocalStorage, IsUserLoggedIn, LoggedInUserTechnicianId, LoggedInUserId } from '../../_helpers/helper';
import UserEditValidator from '../../_helpers/validators/User/UserEditValidator';

const UserEdit = (props) => {
    const [userToEdit, setUserToEdit] = useState({ firstName: "", lastName: "", userName: '', email: '', isActive: false, isUserAdmin: false });
    const [isUserAdmin, setIsUserAdmin] = useState(IsUserLoggedIn() === "Admin");
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [passwordChangeModel, setPasswordChangeModel] = useState({
        password: '',
        confirmPassword: ''
    });

    useEffect(() => {
        GetUserToEdit(props.match.params.id).then(res => {
            setUserToEdit(res);
        });
    }, []);

    const saveEditedUser = (editedUser, setSubmitting) => {
        EditUser(editedUser).then(res => {
            setSubmitting(false);
            Alert({ success: true, message: res.message });

            if (res)
                props.history.push(isUserAdmin ? '/Users' : '/TechnicianDashboard');

        });
    }

    const changePasswordHandler = (pwd) => {
        ChangePassword(props.match.params.id, pwd).then(res => {
            Alert({ success: true, message: res.message });
        }).catch(ex => { })
    }

    return (
        <>
            {/* Change Password Modal */}
            <Modal
                size={"mini"}
                dimmer={true}
                open={isModalOpen}
            >
                <Modal.Header>Change Password</Modal.Header>
                <Formik initialValues={passwordChangeModel}
                    enableReinitialize
                    validationSchema={ChangePasswordValidator}
                    onSubmit={(values, { setSubmitting }) => {
                        changePasswordHandler(values.password);
                        setSubmitting(false);
                        setIsModalOpen(false);
                    }}>
                    {({ values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting }) => (
                            <>
                                <Modal.Content>
                                    <Form onSubmit={handleSubmit}>
                                        <Form.Group widths='equal'>
                                            <Grid divided='vertically'>
                                                <Grid.Row columns={1}>
                                                    <Grid.Column>
                                                        <Form.Group widths='equal'>
                                                            <Form.Field>
                                                                <label>Password</label>
                                                                <Form.Input placeholder='Password'
                                                                    error={touched.password && errors.password}
                                                                    fluid
                                                                    type='text'
                                                                    id='password'
                                                                    onChange={handleChange}
                                                                    value={values.password}
                                                                    onBlur={handleBlur}
                                                                    className={touched.password && errors.password ? "has-error" : null} />
                                                            </Form.Field>
                                                        </Form.Group>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <Form.Group widths='equal'>
                                                            <Form.Field>
                                                                <label>ConfirmPassword</label>
                                                                <Form.Input placeholder='ConfirmPassword'
                                                                    error={touched.confirmPassword && errors.confirmPassword}
                                                                    fluid
                                                                    type='password'
                                                                    id='confirmPassword'
                                                                    onChange={handleChange}
                                                                    value={values.confirmPassword}
                                                                    onBlur={handleBlur}
                                                                    className={touched.confirmPassword && errors.confirmPassword ? "has-error" : null} />
                                                            </Form.Field>
                                                        </Form.Group>
                                                    </Grid.Column>
                                                </Grid.Row>
                                            </Grid>
                                        </Form.Group>
                                    </Form>
                                </Modal.Content>
                                <Modal.Actions>
                                    <Button negative onClick={() => {
                                        setIsModalOpen(false);
                                    }}>
                                        Close
                                    </Button>
                                    <Button positive disabled={isSubmitting} onClick={(handleSubmit)}>
                                        Save
                                    </Button>
                                </Modal.Actions>
                            </>
                        )}
                </Formik>
            </Modal>

            <Formik initialValues={userToEdit}
                validationSchema={UserEditValidator}
                enableReinitialize
                onSubmit={(values, { setSubmitting }) => {
                    setUserToEdit(values);
                    saveEditedUser(values, setSubmitting);
                }}>
                {({ values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting }) => (
                        <React.Fragment>
                            <Header as='h2'>
                                <Header.Content>
                                    Edit User Details
                                    <Header.Subheader>
                                        <Link to='/Users'> Back to listing</Link>
                                    </Header.Subheader>
                                </Header.Content>
                            </Header>
                            <Grid divided='vertically'>
                                <Grid.Row columns={1}>
                                    <Grid.Column>
                                        {
                                            props.match.params.id == LoggedInUserId() ?
                                                <Button floated='right' primary onClick={() => setIsModalOpen(true)}><Icon className="display-on-desktop" name='lock open' />Change Password</Button>
                                                :
                                                <Button floated='right' color='google plus' onClick={() => changePasswordHandler("**")}><Icon name='key' />Reset User Password</Button>
                                        }
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                            <Form onSubmit={handleSubmit}>
                                <Grid divided='vertically'>
                                    <Grid.Row columns={2}>
                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>FirstName</label>
                                                    <Form.Input
                                                        error={touched.firstName && errors.firstName}
                                                        fluid
                                                        placeholder='FirstName'
                                                        type='text'
                                                        id='firstName'
                                                        onChange={handleChange}
                                                        value={values.firstName}
                                                        onBlur={handleBlur}
                                                        className={touched.firstName && errors.firstName ? "has-error" : null}
                                                    />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>LastName</label>
                                                    <Form.Input placeholder='LastName'
                                                        error={touched.lastName && errors.lastName}
                                                        fluid
                                                        type='text'
                                                        id='lastName'
                                                        onChange={handleChange}
                                                        value={values.lastName}
                                                        onBlur={handleBlur}
                                                        className={touched.lastName && errors.lastName ? "has-error" : null} />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>Username</label>
                                                    <Form.Input placeholder='Username'
                                                        error={touched.userName && errors.userName}
                                                        fluid
                                                        type='text'
                                                        id='userName'
                                                        onChange={handleChange}
                                                        value={values.userName}
                                                        onBlur={handleBlur}
                                                        className={touched.userName && errors.userName ? "has-error" : null} />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>Email</label>
                                                    <Form.Input placeholder='Email'
                                                        error={touched.email && errors.email}
                                                        fluid
                                                        type='text'
                                                        id='email'
                                                        onChange={handleChange}
                                                        value={values.email}
                                                        onBlur={handleBlur}
                                                        className={touched.email && errors.email ? "has-error" : null} />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>
                                        {isUserAdmin ?
                                            <Grid.Column>
                                                <Form.Group widths='equal'>
                                                    <Form.Field>
                                                        <Form.Field>
                                                            <Form.Checkbox
                                                                label='Is Admin?'
                                                                id='isUserAdmin'
                                                                onChange={handleChange}
                                                                checked={values.isUserAdmin}
                                                            />
                                                        </Form.Field>
                                                    </Form.Field>
                                                </Form.Group>
                                            </Grid.Column> : ""
                                        }


                                        <Grid.Column style={{ display: isUserAdmin ? 'block' : 'none' }}>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <Form.Field>
                                                        <Form.Checkbox
                                                            label='Is Active?'
                                                            id='isActive'
                                                            onChange={handleChange}
                                                            checked={values.isActive}
                                                        />
                                                    </Form.Field>
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                                <Button type='submit' color='linkedin' size={'medium'} disabled={isSubmitting} floated='right'> <Icon name='save' /> Save</Button>
                            </Form>
                        </React.Fragment>
                    )}
            </Formik>
        </>
    );
}
export default UserEdit;
