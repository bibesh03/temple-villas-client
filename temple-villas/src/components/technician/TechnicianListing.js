import React, { useState, useEffect } from 'react';
import { Container, Modal, Icon, Button, Input } from 'semantic-ui-react';
import DataTable from 'react-data-table-component';
import { GetAllTechnicians, DeleteTechnician } from '../../_services/technician.service';
import { Alert } from '../../_helpers/Alert';

const TechnicianListing = (props) => {
    const [technicianList, setTechnicianList] = useState([]);
    const [searchableList, setSearchableList] = useState([]);

    useEffect(() => {
        GetTechnicianListForTable();
    }, [])

    const GetTechnicianListForTable = () => {
        GetAllTechnicians().then(res => {
            setTechnicianList(res);
            setSearchableList(res);
        }).catch(ex => {
        });
    }

    const DeleteTechnicianFromList = (technicianId) => {
        DeleteTechnician(technicianId).then(res => {
            Alert({ success: true, message: "Success! Selected Technican has been deleted." })
            GetTechnicianListForTable();
        }).catch(ex => {
        });
    }

    const columns = [
        {
            name: '',
            selector: 'id',
            sortable: true,
            cell: row => (
                <>
                    <Button primary size={'tiny'} onClick={() => props.history.push({
                        pathname: 'Technician/' + row.id
                    })}> <Icon name='edit' /><span className="display-on-desktop">Edit</span></Button>
                    <Modal
                        trigger={<Button color='google plus' size={'tiny'}><Icon name='trash' /><span className="display-on-desktop">Delete</span></Button>}
                        header='Delete Technician'
                        content='Are you sure you want to delete this Technician?'
                        actions={['Cancel', { key: 'delete', content: 'Delete', positive: true, onClick:() => DeleteTechnicianFromList(row.id)}]}
                    />
                </>
            )
        },
        {
            name: 'First Name',
            selector: 'firstName',
            sortable: true,
        },
        {
            name: 'Last Name',
            selector: 'lastName',
            sortable: true,
        },
        {
            name: 'Contact Number',
            selector: 'contactNumber',
            sortable: true,
        },
        {
            name: 'Pay Rate',
            selector: 'payRate',
            sortable: true,
            cell: row => '$ ' + row.payRate
        }
    ];

    return (
        <>
            <Button size={'tiny'} floated={'right'} color={'instagram'} onClick={() => props.history.push('/Technician/0')}> <Icon name='add' /> Add Technician</Button>
            <DataTable
                title={"Technicians"}
                columns={columns}
                data={searchableList}
                pagination
                subHeader
                subHeaderAlign="left"
                subHeaderComponent={
                    <React.Fragment>
                        <Input
                            size='small'
                            placeholder='Search by First Name...'
                            onChange={(e, data) => setSearchableList(technicianList.filter(x => x.firstName.toLowerCase().indexOf(data.value.toLowerCase()) > -1))}
                        />
                    </React.Fragment>
                }
            />
        </>
    )
}

export default TechnicianListing;