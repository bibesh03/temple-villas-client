import React, { useState, useEffect } from 'react'
import { Formik } from 'formik'
import TechnicianRegisterValidator from '../../_helpers/validators/Technician/TechnicianRegisterValidator'
import { Grid, Button, Form, Header, Dropdown, Icon, Label, Select } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import { EditTechnician, GetTechnicianToEdit } from '../../_services/technician.service';
import { Alert } from '../../_helpers/Alert';
import { GetNonAdminUsers } from '../../_services/user.service';

const Technician = (props) => {
    const [technicianToSave, setTechnician] = useState({
        id: 0,
        firstName: "",
        lastName: "",
        contactNumber: "",
        payRate: "",
        userId: "",
    });
    const [technicianOptions, setTechnicianOptions] = useState([])

    useEffect(() => {
        GetUsersForDropDown();
    }, []);

    const GetUsersForDropDown = () => {
        GetNonAdminUsers().then(res => {
            var optionsReadyToSet = res.map(x => {
                return {
                    key: x.id,
                    value: x.id,
                    text: x.name,
                }
            });

            if (props.match.params.id > 0) {
                GetUserEditData(optionsReadyToSet);
            } else {
                setTechnicianOptions(optionsReadyToSet);
            }
        }).catch(ex => { })
    }

    const GetUserEditData = (optionsReadyToSet) => {
        GetTechnicianToEdit(props.match.params.id).then(res => {
            optionsReadyToSet.push({
                key: res.userId,
                value: res.userId,
                text: res.user.firstName + " " + res.user.lastName,
            })
            setTechnician(res);
            setTechnicianOptions(optionsReadyToSet);
        }).catch(ex => {
            Alert({ success: false, message: "User does not exist" });
            props.history.push('/Technicians');
        })
    }

    const SaveTechnician = (model, setSubmitting) => {
        EditTechnician(model).then(res => {
            setSubmitting(false);
            Alert({ success: true, message: res.message });
            props.history.push('/Technicians');
        }).catch(ex => {
            setSubmitting(false);
        });
    }

    return (
        <>
            <Formik initialValues={technicianToSave}
                enableReinitialize
                validationSchema={TechnicianRegisterValidator}
                onSubmit={(values, { setSubmitting }) => {
                    SaveTechnician(values, setSubmitting);
                }}>
                {({ values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting }) => (
                        <React.Fragment>
                            <Header as='h2'>
                                <Header.Content>
                                    {props.match.params.id == 0 ? "Add" : "Edit"} Technician
                                    <Header.Subheader>
                                        <Link to='/Technicians'> Back to listing</Link>
                                    </Header.Subheader>
                                </Header.Content>
                            </Header>

                            <Form onSubmit={handleSubmit}>
                                <Grid divided='vertically'>
                                    <Grid.Row columns={2}>
                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>First Name</label>
                                                    <Form.Input
                                                        error={touched.firstName && errors.firstName}
                                                        fluid
                                                        placeholder='FirstName'
                                                        type='text'
                                                        id='firstName'
                                                        onChange={handleChange}
                                                        value={values.firstName}
                                                        onBlur={handleBlur}
                                                        className={touched.firstName && errors.firstName ? "has-error" : null}
                                                    />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>Last Name</label>
                                                    <Form.Input placeholder='LastName'
                                                        error={touched.lastName && errors.lastName}
                                                        fluid
                                                        type='text'
                                                        id='lastName'
                                                        onChange={handleChange}
                                                        value={values.lastName}
                                                        onBlur={handleBlur}
                                                        className={touched.lastName && errors.lastName ? "has-error" : null} />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>Contact Number</label>
                                                    <Form.Input placeholder='Contact Number'
                                                        error={touched.contactNumber && errors.contactNumber}
                                                        fluid
                                                        type='phone'
                                                        id='contactNumber'
                                                        onChange={handleChange}
                                                        value={values.contactNumber}
                                                        onBlur={handleBlur}
                                                        className={touched.contactNumber && errors.contactNumber ? "has-error" : null} />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>Technician</label>
                                                    <Select
                                                        placeholder='Select a Technician'
                                                        fluid
                                                        search
                                                        clearable
                                                        selection
                                                        options={technicianOptions}
                                                        error={(touched.userId && errors.userId) ? true : false}
                                                        id='userId'
                                                        name='userId'
                                                        onChange={(e, elem) => {
                                                            const event = {
                                                                persist: () => { },
                                                                target: {
                                                                    type: "change",
                                                                    id: 'userId',
                                                                    name: 'userId',
                                                                    value: elem.value
                                                                }
                                                            };

                                                            handleChange(event);
                                                        }}
                                                        value={values.userId}
                                                        onBlur={(e, elem) => {
                                                            const event = {
                                                                persist: () => { },
                                                                target: {
                                                                    type: "blur",
                                                                    id: 'userId',
                                                                    name: 'userId',
                                                                    value: elem.value
                                                                }
                                                            };

                                                            handleBlur(event);
                                                        }}
                                                    />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field>
                                                    <label>Pay Rate</label>
                                                    <Form.Input placeholder='Pay Rate'
                                                        error={touched.payRate && errors.payRate}
                                                        fluid
                                                        type='number'
                                                        id='payRate'
                                                        onChange={handleChange}
                                                        value={values.payRate}
                                                        onBlur={handleBlur}
                                                        className={touched.payRate && errors.payRate ? "has-error" : null} />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                                <Button floated='right' type='submit' color='facebook' size={'medium'} disabled={isSubmitting}><Icon name='save' />Save</Button>
                            </Form>
                        </React.Fragment>
                    )}
            </Formik>
        </>
    );
}
export default Technician;
