import React, { useState } from 'react';
import { Button, Form, Image, Loader } from "semantic-ui-react";
import { Formik } from 'formik';
import UserLoginValidator from '../../_helpers/validators/User/UserLoginValidator';
import { Authenticate } from '../../_services/user.service';
import { SaveItemInLocalStorage } from '../../_helpers/helper';
import { IsUserLoggedIn } from '../../_helpers/helper';
import Logo from '../../assets/img/TempleVillas-Logo-600.png';

const Login = (props) => {
    const [user, setUser] = useState({ email: "", password: "" });

    const AuthenticateUser = (values, setSubmitting) => {
        Authenticate(values).then(res => {
            SaveItemInLocalStorage("user", res);
            setSubmitting(false);
            props.history.push(IsUserLoggedIn() === "Admin" ? "/" : "/TechnicianDashboard");
        }).catch(ex => {
            setSubmitting(false);
        })
    }

    return (
        <div className="mainContainer">
            <div className="formContainer">
                <div className="imageBlock">
                    <Image
                        src={Logo}
                        // size="medium"
                        className="logo"
                    />
                </div>
                <Formik initialValues={user}
                    validationSchema={UserLoginValidator}
                    onSubmit={(values, { setSubmitting }) => {
                        AuthenticateUser(values, setSubmitting);
                    }}
                >
                    {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                            <div className="formBlock">
                                <Form onSubmit={handleSubmit} id="login-form">
                                    <Form.Field>
                                        <label>Email/Username</label>
                                        <Form.Input
                                            placeholder="Enter username or email"
                                            id="email"
                                            onChange={handleChange}
                                            value={values.email}
                                            onBlur={handleBlur}
                                            // style={{ minWidth: '416.11px' }}
                                            icon='user'
                                            iconPosition='left'
                                            error={touched.email && errors.email}
                                        />
                                    </Form.Field>
                                    <Form.Field>
                                        <label>Password</label>
                                        <Form.Input
                                            id='password'
                                            placeholder='Enter password'
                                            onChange={handleChange}
                                            value={values.password}
                                            type='password'
                                            icon='key'
                                            iconPosition='left'
                                            onBlur={handleBlur}
                                            error={touched.password && errors.password}
                                            // style={{ minWidth: '416.11px' }}
                                        />
                                    </Form.Field>

                                    <Button disabled={isSubmitting} type="submit" id="login-btn">
                                        {isSubmitting ? <Loader active inline='centered' /> : "Login"}
                                    </Button>
                                </Form>
                            </div>
                        )}
                </Formik>
            </div>
        </div>
    )
}

export default Login;