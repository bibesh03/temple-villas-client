import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';
import { Container, Header, Icon, Button, Input, Loader, Modal } from 'semantic-ui-react';
import { DeleteWorkOrder, EditWorkOrder, GetAllWorkOrdersByType } from '../../_services/workorder.service';
import { GetJsonFromLocalStorage, LoggedInUserTechnicianId } from '../../_helpers/helper';
import { Alert } from '../../_helpers/Alert';
import { DownloadReport } from '../../_helpers/helper';

const TechnicianDashboard = (props) => {
    const [pendingWorkOrderList, setPendingWorkOrderList] = useState([]);
    const [searchablePendingWorkOrderList, setSearchablePendingWorkOrderList] = useState([]);
    const [completedWorkOrderList, setCompletedWorkOrderList] = useState([]);
    const [searchableCompletedWorkOrderList, setSearchableCompletedWorkOrderList] = useState([]);
    const [processing, setIsProcessing] = useState(false);
    const loggedInUserId = GetJsonFromLocalStorage('user').id;

    useEffect(() => {
        PopulatePendingtable();
        PopulateCompletedtable();
    }, [])

    const PopulatePendingtable = () => {
        GetAllWorkOrdersByType(loggedInUserId, false).then(res => {
            setPendingWorkOrderList(res);
            setSearchablePendingWorkOrderList(res);
        }).catch(() => { });
    }

    const PopulateCompletedtable = () => {
        GetAllWorkOrdersByType(loggedInUserId, true).then(res => {
            setCompletedWorkOrderList(res);
            setSearchableCompletedWorkOrderList(res);
        }).catch(() => { });
    }

    const CreateTicketAndNavigateToTicket = () => {
        setIsProcessing(true);
        EditWorkOrder({
            id: 0,
            technicianId: LoggedInUserTechnicianId(),
            roomId: null,
            dateStarted: null,
            dateCompleted: null,
            rate: null,
            createdBy: loggedInUserId,
            createdDate: new Date()
        }).then(res => {
            props.history.push({
                pathname: 'WorkOrder/' + res.id
            });
            setIsProcessing(false);
        }).catch(ex => {
            setIsProcessing(false);
        })
    }

    const DeleteWorkOrderFromModal = (workOrderId) => {
        DeleteWorkOrder(workOrderId).then(res => {
            Alert({ success: true, message: res.message});
            PopulatePendingtable();
            PopulateCompletedtable();
        }).catch(res => {})
    }

    const columns = [
        {
            name: '',
            selector: 'id',
            sortable: true,
            cell: row => <>
                <Button primary size={'tiny'} onClick={() => props.history.push({
                    pathname: 'WorkOrder/' + row.id
                })}> <Icon name='edit' /> Edit
                </Button>
                <Modal
                    trigger={<Button color='google plus' size={'tiny'}> <Icon name='trash' /> Delete</Button>}
                    header='Delete Work Order'
                    content='Are you sure you want to delete this Work Order? All the line items associated with work orders will get deleted on the process.'
                    actions={['Cancel', { key: 'delete', content: 'Delete', positive: true, onClick: () => DeleteWorkOrderFromModal(row.id) }]}
                />

            </>
        },
        {
            name: 'Work Order#',
            selector: 'workOrderId',
            sortable: true,
        },
        {
            name: 'Location',
            selector: 'location',
            sortable: true,
            grow: 2,
        },
        {
            name: 'Date Started',
            selector: 'dateStarted',
            sortable: true,
        },
        {
            name: 'Date Completed',
            selector: 'dateCompleted',
            sortable: true,
        },
        // {
        //     name: 'Rate',
        //     selector: 'rate',
        //     sortable: false,
        //     cell: row => row.rate ? '$' + row.rate : "-"
        // }
    ];

    const completed_columns = [
        {
            name: '',
            selector: 'id',
            sortable: true,
            cell: row => <>
                <Button primary size={'mini'} onClick={() => props.history.push({
                    pathname: 'WorkOrder/' + row.id
                })}> <Icon name='eye' /> View
                </Button>
                <Button secondary size={'mini'} onClick={() => DownloadReport(row.id)}> <Icon name='print' /> Print
                </Button>
            </>
        },
        {
            name: 'Work Order#',
            selector: 'workOrderId',
            sortable: true,
        },
        {
            name: 'Location',
            selector: 'location',
            grow: 3,
            sortable: true,
        },
        {
            name: 'Date Started',
            selector: 'dateStarted',
            sortable: true,
        },
        {
            name: 'Date Completed',
            selector: 'dateCompleted',
            sortable: true,
        },
        // {
        //     name: 'Rate',
        //     selector: 'rate',
        //     sortable: true,
        //     cell: row => row.rate ? '$' + row.rate : "-"
        // },
        {
            name: 'Submitted Date',
            selector: 'completedDate',
            sortable: true,
        }
    ];

    return (
        <div>
            <h1>Dashboard</h1>
            <Button disabled={processing} size={'tiny'} floated={'right'} color={'instagram'} onClick={() => CreateTicketAndNavigateToTicket()}>
                {processing ? <Loader active inline='centered' size='tiny' /> : <><Icon name='add' /> Create New Work Order </>}
            </Button>
            <DataTable
                title={"Pending Work Orders"}
                columns={columns}
                data={searchablePendingWorkOrderList}
                pagination
                subHeader
                subHeaderAlign="left"
                subHeaderComponent={
                    <React.Fragment>
                        <Input
                            size='small'
                            placeholder='Search by Work Order #'
                            onChange={(e, data) => setSearchablePendingWorkOrderList(pendingWorkOrderList.filter(x => x.workOrderId.toLowerCase().indexOf(data.value.toLowerCase()) > -1))}
                        />
                    </React.Fragment>
                }
            />

            <DataTable
                title={"Completed Work Orders"}
                columns={completed_columns}
                data={searchableCompletedWorkOrderList}
                pagination
                subHeader
                subHeaderAlign="left"
                subHeaderComponent={
                    <React.Fragment>
                        <Input
                            size='small'
                            placeholder='Search by Work Order #'
                            onChange={(e, data) => setSearchableCompletedWorkOrderList(completedWorkOrderList.filter(x => x.workOrderId.toLowerCase().indexOf(data.value.toLowerCase()) > -1))}
                        />
                    </React.Fragment>
                }
            />
        </div>
    )
}

export default TechnicianDashboard;