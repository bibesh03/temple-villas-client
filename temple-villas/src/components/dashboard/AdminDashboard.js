import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';
import { Icon, Button, Input, Loader, Modal } from 'semantic-ui-react';
import { DeleteWorkOrder, GetAllWorkOrders } from '../../_services/workorder.service';
import { Alert } from '../../_helpers/Alert';
import { DownloadReport } from '../../_helpers/helper';

const AdminDashboard = (props) => {
    const [pendingWorkOrderList, setPendingWorkOrderList] = useState([]);
    const [searchablePendingWorkOrderList, setSearchablePendingWorkOrderList] = useState([]);
    const [completedWorkOrderList, setCompletedWorkOrderList] = useState([]);
    const [searchableCompletedWorkOrderList, setSearchableCompletedWorkOrderList] = useState([]);
    const [pendingIsLoading, setPendingIsLoading] = useState(true);
    const [completedIsLoading, setCompletedIsLoading] = useState(true);

    useEffect(() => {
        PopulateTables();
    }, [])

    const PopulateTables = () => {
        GetAllWorkOrders().then(res => {
            let pendingWorkOrders = res.filter(x => x.isCompleted == false);
            let completedWorkOrders = res.filter(x => x.isCompleted == true);

            setPendingWorkOrderList(pendingWorkOrders);
            setSearchablePendingWorkOrderList(pendingWorkOrders);
            setPendingIsLoading(false);

            setCompletedWorkOrderList(completedWorkOrders);
            setSearchableCompletedWorkOrderList(completedWorkOrders);
            setCompletedIsLoading(false);
        }).catch(() => { });
    }

    const DeleteWorkOrderFromModal = (workOrderId) => {
        DeleteWorkOrder(workOrderId).then(res => {
            Alert({ success: true, message: res.message });
            setPendingIsLoading(true);
            setCompletedIsLoading(true);

            PopulateTables();
        }).catch(res => { })
    }

    const columns = [
        {
            name: '',
            selector: 'id',
            sortable: true,
            grow: 2,
            cell: row => <>
                <Button primary size={'tiny'} onClick={() => props.history.push({
                    pathname: 'WorkOrder/' + row.id
                })}> <Icon name='edit' /><span className="display-on-desktop">Edit</span>
                </Button>
                <Modal
                    trigger={<Button color='google plus' size={'tiny'}> <Icon name='trash' /><span className="display-on-desktop">Delete</span></Button>}
                    header='Delete Work Order'
                    content='Are you sure you want to delete this Work Order? All the line items associated with work orders will get deleted on the process.'
                    actions={['Cancel', { key: 'delete', content: 'Delete', positive: true, onClick: () => DeleteWorkOrderFromModal(row.id) }]}
                />

            </>
        },
        {
            name: 'Work Order#',
            selector: 'workOrderId',
            sortable: true,
        },
        {
            name: 'Technician',
            selector: 'technician',
            sortable: true,
        },
        {
            name: 'Location',
            selector: 'location',
            sortable: true,
            grow: 2,
        },
        {
            name: 'Date Started',
            selector: 'dateStarted',
            sortable: true,
        },
        {
            name: 'Date Completed',
            selector: 'dateCompleted',
            sortable: true,
        },
        {
            name: 'Rate',
            selector: 'rate',
            sortable: false,
            cell: row => row.rate ? '$' + row.rate : "-"
        }
    ];

    const completed_columns = [
        {
            name: '',
            selector: 'id',
            sortable: true,
            grow: 1.25,
            cell: row => <>
                <Button primary size={'mini'} onClick={() => props.history.push({
                    pathname: 'WorkOrder/' + row.id
                })}> <Icon name='eye' /><span className="display-on-desktop">View</span>
                </Button>
                <Button secondary size={'mini'} onClick={() => DownloadReport(row.id)}> <Icon name='print' /><span className="display-on-desktop">Print</span>
                </Button>
            </>
        },
        {
            name: 'Work Order#',
            selector: 'workOrderId',
            sortable: true,
        },
        {
            name: 'Technician',
            selector: 'technician',
            sortable: true,
        },
        {
            name: 'Location',
            selector: 'location',
            grow: 2,
            sortable: true,
        },
        {
            name: 'Date Started',
            selector: 'dateStarted',
            sortable: true,
        },
        {
            name: 'Date Completed',
            selector: 'dateCompleted',
            sortable: true,
        },
        {
            name: 'Rate',
            selector: 'rate',
            sortable: true,
            cell: row => row.rate ? '$' + row.rate : "-"
        },
        {
            name: 'Submitted Date',
            selector: 'completedDate',
            sortable: true,
        }
    ];

    return (
        <div>
            <Button size={'tiny'} floated={'right'} color={'instagram'} onClick={() => props.history.push({
                pathname: 'WorkOrder/' + 0
            })}>
                <><Icon name='add' /> Create New Work Order </>
            </Button>
            <DataTable
                title={"Pending Work Orders"}
                columns={columns}
                data={searchablePendingWorkOrderList}
                pagination
                subHeader
                subHeaderAlign="left"
                progressPending={pendingIsLoading}
                progressComponent={<Loader active inline='centered' />}
                subHeaderComponent={
                    <React.Fragment>
                        <Input
                            size='small'
                            placeholder='Search by Work Order #'
                            onChange={(e, data) => setSearchablePendingWorkOrderList(pendingWorkOrderList.filter(x => x.workOrderId.toLowerCase().indexOf(data.value.toLowerCase()) > -1))}
                        />
                    </React.Fragment>
                }
            />

            <DataTable
                title={"Completed Work Orders"}
                columns={completed_columns}
                data={searchableCompletedWorkOrderList}
                pagination
                subHeader
                subHeaderAlign="left"
                progressPending={completedIsLoading}
                progressComponent={<Loader active inline='centered' />}
                subHeaderComponent={
                    <React.Fragment>
                        <Input
                            size='small'
                            placeholder='Search by Work Order #'
                            onChange={(e, data) => setSearchableCompletedWorkOrderList(completedWorkOrderList.filter(x => x.workOrderId.toLowerCase().indexOf(data.value.toLowerCase()) > -1))}
                        />
                    </React.Fragment>
                }
            />
        </div>
    )
}

export default AdminDashboard;