import React, {  useState } from 'react'
import { Dropdown, Menu, Container,Responsive } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import { GetJsonFromLocalStorage, IsUserLoggedIn } from '../../_helpers/helper';

const NavBar = (props) => {
    const [isUserAdmin, setIsUserAdmin] = useState(IsUserLoggedIn() === "Admin");

    const options = isUserAdmin ?
        [
            { key: 'dashboard', text: 'Dashboard', icon: 'dashboard', value: '/' },
            { key: 'user', text: 'Account', icon: 'user', value: '/User/' + GetJsonFromLocalStorage('user').id },
            { key: 'settings', text: 'Users', icon: 'settings', value: '/Users' },
            { key: 'sign-out', text: 'Sign Out', icon: 'sign out', value: 'SignOut' },
        ] : [
            { key: 'technicianDashboard', text: 'Dashboard', icon: 'dashboard', value: '/technicianDashboard' },
            { key: 'user', text: 'Account', icon: 'user', value: '/User/' + GetJsonFromLocalStorage('user').id },
            { key: 'sign-out', text: 'Sign Out', icon: 'sign out', value: 'SignOut' }
        ]

    const logOut = () => {
        localStorage.removeItem('user');
        props.history.push('/');
    }

    const handleRoute = (option) => {
        if (option.value === 'SignOut') {
            logOut();
            return;
        }

        props.history.push(option.value);
    }

    return (
        <Responsive>
            <Menu inverted style={{ marginBottom: '40px', background: 'linear-gradient(45deg, #bc2b2b, #d66f4a)', borderRadius: 0 }}>
                <Container className="adjusted-container">
                    <Menu.Item header position='left'>
                        <Link to={isUserAdmin ? '/' : "/TechnicianDashboard"}>
                            <span style={{ fontSize: '20px' }}>Temple Villas</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item position='right'>
                        <Dropdown floating value="" options={options} text={"Hello, " + GetJsonFromLocalStorage('user').username} onChange={(event, elem) => {
                            handleRoute(elem);
                        }} />
                    </Menu.Item>
                </Container>
            </Menu>
            {
                props.hasNavigation ?
                    <Container className="adjusted-container">
                        <Menu pointing style={{ marginBottom: '18px' }}>
                            <Menu.Item
                                name='Dashboard'
                                active={props.location.pathname === '/'}
                                onClick={() => props.history.push('/')}
                                className="tab-menu"
                            />
                            <Menu.Item
                                name='Technicians'
                                active={props.location.pathname === '/Technicians'}
                                onClick={() => props.history.push('/Technicians')}
                                className="tab-menu"
                            />
                            <Menu.Item
                                name='Locations'
                                active={props.location.pathname === '/Locations'}
                                onClick={() => props.history.push('/Locations')}
                                className="tab-menu"
                            />
                            <Menu.Item
                                name='Reports'
                                active={props.location.pathname === '/Reports'}
                                onClick={() => props.history.push('/Reports')}
                                className="tab-menu"
                            />
                        </Menu>
                    </Container> : ""
            }
        </Responsive>
    )
}

export default NavBar;