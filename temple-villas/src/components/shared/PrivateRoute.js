import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { IsUserLoggedIn } from '../../_helpers/helper';
import NavBar from './NavBar';
import { Container, Responsive } from 'semantic-ui-react';

export const PrivateRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={props => {
            const isAuthenticated = rest.role.split(',').includes(IsUserLoggedIn());

            if ((rest.path == "/" || rest.path == "/dashboard") && IsUserLoggedIn() === "Technician" ) {
                return <Redirect to={{ pathname: '/TechnicianDashboard', state: { from: props.location } }} />
            }

            if (isAuthenticated) {
                return (
                    <>
                        <NavBar {...props} hasNavigation={rest.hasNavigation} />
                        <Responsive>
                            <Container className="adjusted-container">
                                <Component {...props} />
                            </Container>
                        </Responsive>
                    </>
                )
            } else if (IsUserLoggedIn() == "Technician") {
                return <Redirect to={{ pathname: '/technicianDashboard', state: { from: props.location } }} />
            }

            return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        }} />
    )
}