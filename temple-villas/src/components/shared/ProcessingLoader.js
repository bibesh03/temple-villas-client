import React from 'react';
import { Segment, Image, Dimmer, Loader } from 'semantic-ui-react';

const ProcessingLoader = () => {
    return (
        <Segment>
            <Dimmer active inverted>
                <Loader size='massive'>Loading</Loader>
            </Dimmer>

            <Image src='https://react.semantic-ui.com/images/wireframe/paragraph.png' />
            <Image src='https://react.semantic-ui.com/images/wireframe/paragraph.png' />
            <Image src='https://react.semantic-ui.com/images/wireframe/paragraph.png' />
        </Segment>
    )
}

export default ProcessingLoader;