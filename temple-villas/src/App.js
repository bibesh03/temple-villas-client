import React, { useEffect } from 'react';
import { Router, Route, Switch } from "react-router-dom";
import { ToastContainer } from 'react-toastify';
import { createBrowserHistory } from "history";
import Login from './components/login/Login';
import { IsUserLoggedIn } from './_helpers/helper';
import { PrivateRoute } from './components/shared/PrivateRoute';
import AdminDashboard from './components/dashboard/AdminDashboard';
import TechnicianListing from './components/technician/TechnicianListing';
import LocationListing from './components/location/LocationListing';
import Report from './components/report/Report';
import UserListing from './components/user/UserListing';
import Register from './components/user/Register';
import UserEdit from './components/user/UserEdit';
import Technician from './components/technician/Technician';
import TechnicianDashboard from './components/dashboard/TechnicianDashboard';
import WorkOrder from './components/workorder/WorkOrder.js'
import 'react-toastify/dist/ReactToastify.css';
import "semantic-ui-css/semantic.min.css";
import './assets/styles/login.css';
import './assets/styles/style.css';

const hist = createBrowserHistory();

const App = () => {
  useEffect(() => {
    console.log("Running", process.env);

    IsUserLoggedIn();
  }, [])
  return (
    <Router history={hist}>
      <Switch>
        <Route path="/Login" component={Login} />
        <PrivateRoute path="/TechnicianDashboard" component={TechnicianDashboard} role="Technician" hasNavigation={false} />
        <PrivateRoute exact path="/" component={AdminDashboard} role="Admin" hasNavigation={true} />
        <PrivateRoute path="/Technicians" component={TechnicianListing} role="Admin" hasNavigation={true} />
        <PrivateRoute path="/Locations" component={LocationListing} role="Admin" hasNavigation={true} />
        <PrivateRoute path="/Users" component={UserListing} role="Admin" hasNavigation={false} />
        <PrivateRoute path="/Register" component={Register} role="Admin" hasNavigation={false} />
        <PrivateRoute path="/User/:id" component={UserEdit} role="Admin,Technician" hasNavigation={false} />
        <PrivateRoute path="/Technician/:id" component={Technician} role="Admin" hasNavigation={false} />
        <PrivateRoute path="/WorkOrder/:id" component={WorkOrder} role="Admin,Technician" hasNavigation={false} />
        <PrivateRoute path="/Reports" component={Report} role="Admin" hasNavigation={true} />
      </Switch>
      <ToastContainer
        position="top-right"
        autoClose={4500}
        draggable
        closeOnClick
      />
    </Router>
  );
}

export default App;
